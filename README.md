# Key frame editor
**Disclaimer**: the text input is bugged and doesn't allow for usage of backspace. Please fully select the number and overwrite it when making a typo...

**Disclaimer 2**: the application has only been tested with OpenGL 4.6 on Linux. Other platforms and versions might not compile/run.

## Compilation
This project was written in Rust. Follow the steps bellow to build and run the application. More info on installing Rust can be found [on their website](https://www.rust-lang.org/tools/install):
- Install the Rust language tool chain manager: `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`
- Install the stable tool chain: `rustup toolchain install stable`
- Navigate to the root directory of the project (the directory containing this file).
- Download all dependencies and build the application: `cargo +stable build --release`
- Run the application: `cargo +stable run --release`, or invoke the binary directly `./target/release/keyframe_editor`

## Binary
For the teaching staff's convenience, a binary is already included. Simply invoke the `./keyframe_editor` file.

**Note**: when moving the binary to a different directory, move the `./resources` folder with it!

## Usage
### Editor camera
When not running the simulation, the camera can be controlled as follows:
- __W/A/S/D__: forwards/left/backwards/right
- __Q/E__: down/up
- __up/left/down/right__ (arrow keys): turn into the specified direction
- __PageUp/PageDown__: tilt left/right

### Load/Save
![](md_img/2020-08-28-19-16-31.png)

Saves or loads a scene (all key frames, models, etc.). The save file is called `keyframe_savefile.json`. It is stored in the directory from which the application was launched.

An example save file, with the name `keyframe_savefile.example.json`, is provided in the root of the project.

### Running the simulation
![](md_img/2020-08-28-19-26-13.png)

- __Start/Stop__: play/pause the simulation.
- __Reset__: reset the simulation progress to `t = 0`.
- __FPS__: set the FPS at which the simulation has to run. Only takes effect when actually running, not when paused (i.e. "editor mode").

### Channel selection
![](md_img/2020-08-28-19-29-19.png)

Changes the editor to show options for the editing of a specific channel. Channel types are:
- __Camera path__: the path to be taken by the camera during simulation
- __Skybox__: set the skybox (skyboxes are provided in `./resources/`)
- __Step Ahead__: step ahead animation. Allows you to load a model and specify how it behaves at given key frames.

### Camera path editor
![](md_img/2020-08-28-19-33-29.png)

Set the speed function to be taken when evaluating the path. 

![](md_img/2020-08-28-19-33-59.png)

Add/edit the key frames (i.e. "checkpoints") of the camera path. When clicking on either of these buttons, the following pop-up opens.

![](md_img/2020-08-28-19-35-09.png)

Set the values of the camera path key frame here.
- `t` is the sample time at which this key frame will be passed
- rotation is done by specifying a view target (bottom 3 fields)

### Step ahead editor
![](md_img/2020-08-28-19-39-12.png)

Browse for an `.obj` file and load the model. Textures are loaded as well if available. Due to time limitations, only `.obj` files which use vertex triplets are supported. (Some `.obj` files use quadruplets in the file format.)

Example models are provided in the `./assets/model/` directory.

![](md_img/2020-08-28-19-44-44.png)

Once a model is loaded, adding and editing key frames can be done similarly to editing the camera path's key frames. 

**Note**: unlike the camera path key frames, rotation is defined as euler angles directly on the object.

![](md_img/2020-08-28-19-48-55.png)

After adding key frames with the buttons above, the model can be manipulated by means of manipulating its FFD grid at said key frames. Key frames are listed by sample time. Selecting one will change the editor to grid manipulation mode, as shown bellow.

![](md_img/2020-08-28-19-53-50.png)

**Note**: the grid editor view hides all other models. To see them, select the "Don't edit" option again in the "Edit FFD" dropdown.