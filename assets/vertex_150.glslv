#version 330 core

in vec4  a_Pos;
in vec4  a_Normal;
in vec4  a_ColorDiffuse;
in vec4  a_ColorSpecular;
in vec2  a_TexCoord;
out vec4 v_WorldPos;
out vec4 v_WorldNormal;
out vec4 v_ColorDiffuse;
out vec4 v_ColorSpecular;
out vec4 v_Eye;
out vec2 v_TexCoord;

uniform Locals
{
    mat4 model_mat;
    mat4 view_mat;
    mat4 proj_mat;
    vec4 eye;
};

void main()
{
    gl_Position = proj_mat * view_mat * model_mat * a_Pos;

    v_WorldPos    = model_mat * a_Pos;
    v_WorldNormal = model_mat * a_Normal;

    v_Eye           = eye;
    v_TexCoord      = a_TexCoord;
    v_ColorDiffuse  = a_ColorDiffuse;
    v_ColorSpecular = a_ColorSpecular;

    gl_ClipDistance[0] = 1.0;
    gl_PointSize       = 3.0;
}