#version 330 core

in vec4 v_WorldPos;
in vec4 v_WorldNormal;
in vec4 v_ColorDiffuse;
in vec4 v_ColorSpecular;
in vec4 v_Eye;
in vec2 v_TexCoord;

out vec4 out_color;

uniform sampler2D diffuse_texture;

// TODO: input from engine
const vec4 LIGHT_POSITION = vec4(5.0, 5.0, 0.0, 1.0);
const vec4 LIGHT_COLOUR   = vec4(1.0);

const vec4 AMBIENT = vec4(0.1, 0.1, 0.1, 1.0);

void main()
{
    // diffuse
    vec4 normal          = normalize(v_WorldNormal);
    vec4 light_direction = normalize(LIGHT_POSITION - v_WorldPos);
    vec4 diffuse         = max(dot(normal, light_direction), 0.0) * LIGHT_COLOUR
                   * v_ColorDiffuse;

    // specular
    vec4  look_direction = normalize(v_Eye - v_WorldPos);
    vec4  half_normal    = normalize(light_direction + look_direction);
    float NdotH          = max(dot(normal, half_normal), 0.0);

    vec4 specular = pow(NdotH, v_ColorSpecular.a) * LIGHT_COLOUR
                    * vec4(v_ColorSpecular.rgb, 0.0);

    // output
    out_color
        = (AMBIENT + diffuse + specular) * texture(diffuse_texture, v_TexCoord);
}