use std::env;

fn main()
{
    let mut copy_opts = fs_extra::dir::CopyOptions::new();
    copy_opts.overwrite = true;

    let out_dir = format!(
        "{}/../../../",
        env::var("OUT_DIR").expect("Unable to determine build output directory.")
    );

    fs_extra::dir::copy("./resources", &out_dir, &copy_opts)
        .expect("Failed to copy resources dir.");
    fs_extra::dir::copy("./assets", &out_dir, &copy_opts).expect("Failed to copy assets dir.");
}
