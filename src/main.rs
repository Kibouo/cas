#[macro_use]
extern crate gfx;
#[macro_use]
extern crate imgui;
#[macro_use]
extern crate derive_deref;
#[macro_use]
extern crate anyhow;
#[macro_use]
extern crate num_derive;
#[macro_use]
extern crate serde_derive;

mod channel;
mod engine;
mod example_cube;
mod image;
mod type_aliases;

use engine::engine_state::EngineState;
use ggez::{event, GameResult};
use std::path;

// Note to myself: next time, use an ECS to prevent messy passing of objects.

pub fn main() -> GameResult
{
    let resource_dir = path::PathBuf::from("./resources");

    let cb = ggez::ContextBuilder::new("keyframe_editor", "csonka_mihaly")
        .add_resource_path(resource_dir);

    let (ctx, events_loop) = &mut cb.build()?;
    ggez::graphics::set_window_title(ctx, "Keyframe Editor");

    let state = &mut EngineState::new(ctx);

    event::run(ctx, events_loop, state)
}
