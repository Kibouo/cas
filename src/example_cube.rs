use crate::{
    engine::{colored_texture, renderable::TryIntoRenderData, Color},
    type_aliases::{Matrix4f, TextureWithSampler, VerticesWithIndices}
};
use gfx::traits::FactoryExt;
use ggez::graphics::{BackendSpec, GlBackendSpec};

#[derive(Default)]
pub struct DebugCube;

impl TryIntoRenderData for DebugCube
{
    fn vertex_data(
        &self,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Option<VerticesWithIndices>
    {
        let (vertex_data, index_data) = crate::engine::cube_vertex_data(1.0, false, false);
        Some(factory.create_vertex_buffer_with_slice(&vertex_data, &index_data[..]))
    }

    fn texture_data(
        &self,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Option<TextureWithSampler>
    {
        Some(colored_texture(factory, Color::Blue))
    }

    fn model_mat(&self) -> Matrix4f { Matrix4f::default() }
}
