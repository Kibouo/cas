pub mod speed_fn;

use super::key_frame::KeyFrame;
use crate::{
    engine::{colored_texture, renderable::TryIntoRenderData, vertex, Color},
    type_aliases::*
};
use anyhow::Result;
use decorum::R32;
use gfx::traits::FactoryExt;
use ggez::graphics;
use graphics::{BackendSpec, GlBackendSpec};
use num_traits::FromPrimitive;
use speed_fn::SpeedFn;
use splines::Interpolation;
use std::cmp::{max, min};

const EVAL_STEP_SIZE: f32 = 0.02;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Path
{
    translation_path: Spline3f,
    rotation_path:    Spline3f,
    speed_fn:         usize
}

impl Default for Path
{
    fn default() -> Self
    {
        let default_keyframe = Key3f::new(0.0, *Vector3f::default(), Interpolation::Cosine);
        Path {
            translation_path: Spline3f::from_vec(vec![default_keyframe]),
            rotation_path:    Spline3f::from_vec(vec![default_keyframe]),
            speed_fn:         SpeedFn::None as usize
        }
    }
}

impl Path
{
    pub fn speed_fn_mut(&mut self) -> &mut usize { &mut self.speed_fn }

    pub fn add_keyframe(&mut self, keyframe: KeyFrame) -> &mut Self
    {
        if self
            .translation_path
            .keys()
            .iter()
            .find(|k| k.t == keyframe.t)
            .is_some()
        {
            self
        }
        else {
            self.translation_path.add(Key3f::new(
                keyframe.t,
                *keyframe.translation.inner(),
                Interpolation::Cosine
            ));
            self.rotation_path.add(Key3f::new(
                keyframe.t,
                *keyframe.rotation.inner(),
                Interpolation::Cosine
            ));

            self
        }
    }

    pub fn get(&self, i: usize) -> Option<KeyFrame>
    {
        self.translation_path
            .get(i)
            .and_then(|k| self.rotation_path.get(i).map(|r| (k, r)))
            .map(|(translation_key, rotation_key)| KeyFrame::from((translation_key, rotation_key)))
    }

    pub fn clamped_sample(&self, t: SampleTime) -> Result<KeyFrame>
    {
        let max_t = self.max_t();
        let speedy_t = self.apply_speed_fn(min(R32::from(t), R32::from(max_t)).into());

        let translation = self
            .translation_path
            .clamped_sample(speedy_t)
            .ok_or_else(|| {
                anyhow!(
                    "No translation for t = {}. After passing through speed_fn = {}",
                    t,
                    speedy_t
                )
            })?;
        let rotation = self.rotation_path.clamped_sample(speedy_t).ok_or_else(|| {
            anyhow!(
                "No translation for t = {}. After passing through speed_fn = {}",
                t,
                speedy_t
            )
        })?;

        Ok(KeyFrame::from((
            &Key3f::new(t, translation, Interpolation::Cosine),
            &Key3f::new(t, rotation, Interpolation::Cosine)
        )))
    }

    pub fn keys<'a>(&'a self) -> impl Iterator<Item = KeyFrame> + 'a
    {
        self.translation_path
            .keys()
            .iter()
            .zip(self.rotation_path.keys())
            .map(KeyFrame::from)
    }

    pub fn len(&self) -> usize { self.translation_path.len() }

    pub fn update_from_keyframes<'a>(
        &mut self,
        path: impl Iterator<Item = &'a KeyFrame> + 'a
    ) -> &mut Self
    {
        let (translation_path, rotation_path) = path.fold(
            (Vec::new(), Vec::new()),
            |(mut translation_path, mut rotation_path), keyframe| {
                translation_path.push(Key3f::new(
                    keyframe.t,
                    *keyframe.translation.inner(),
                    Interpolation::Cosine
                ));
                rotation_path.push(Key3f::new(
                    keyframe.t,
                    *keyframe.rotation.inner(),
                    Interpolation::Cosine
                ));
                (translation_path, rotation_path)
            }
        );

        self.translation_path = Spline3f::from_vec(translation_path);
        self.rotation_path = Spline3f::from_vec(rotation_path);

        self
    }

    fn apply_speed_fn(&self, t: SampleTime) -> f32
    {
        match FromPrimitive::from_usize(self.speed_fn).unwrap() {
            SpeedFn::None => t,
            SpeedFn::EaseInOut => {
                let max_t = self.max_t();

                let t = t / max_t;
                let t = t.powf(2 as SampleTime)
                    / (t.powf(2 as SampleTime) + (1 as SampleTime - t).powf(2 as SampleTime));

                t * max_t
            }
        }
    }

    fn max_t(&self) -> SampleTime
    {
        let amt_frames = self.translation_path.len() as isize;
        self.translation_path
            .get(max(amt_frames - 1, 0) as usize)
            .map(|key| key.t)
            .unwrap_or_default() as SampleTime
    }
}

impl TryIntoRenderData for Path
{
    fn vertex_data(
        &self,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Option<VerticesWithIndices>
    {
        if self.translation_path.len() == 0 {
            None
        }
        else {
            let end_sec_of_path = self
                .translation_path
                .keys()
                .get(self.translation_path.len() - 1)
                .map(|key| key.t)
                .unwrap_or_default();
            let indices =
                (0..=(end_sec_of_path * (1.0 / EVAL_STEP_SIZE)) as u32).collect::<Vec<_>>();

            if indices.len() == 0 {
                None
            }
            else {
                let vertex_data: Vec<vertex::Vertex> = indices
                    .iter()
                    .map(|eval_t| self.apply_speed_fn(*eval_t as f32 * EVAL_STEP_SIZE))
                    .filter_map(|eval_t| self.translation_path.clamped_sample(eval_t))
                    .map(vertex::Vertex::from)
                    .collect();

                Some(factory.create_vertex_buffer_with_slice(&vertex_data[..], &indices[..]))
            }
        }
    }

    fn texture_data(
        &self,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Option<TextureWithSampler>
    {
        Some(colored_texture(factory, Color::Green))
    }

    fn model_mat(&self) -> Matrix4f { Matrix4f::default() }
}
