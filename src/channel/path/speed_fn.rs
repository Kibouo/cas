use imgui::ImStr;
use std::path::{Path, PathBuf};
#[derive(FromPrimitive, Copy, Clone)]
pub enum SpeedFn
{
    None,
    EaseInOut
}

pub fn speed_fn_ui_list() -> Vec<&'static ImStr> { vec![im_str!("None"), im_str!("Ease-in/out")] }

pub fn speed_fn_graph_img_file_paths(resources_dir: PathBuf) -> Vec<PathBuf>
{
    ["speed_fn_linear.png", "speed_fn_ease.png"]
        .iter()
        .map(|img| resources_dir.join(Path::new(&img)))
        .collect()
}
