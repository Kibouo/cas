use crate::type_aliases::{Key3f, SampleTime, Vector3f};

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct KeyFrame
{
    pub t:           SampleTime,
    pub translation: Vector3f,
    pub rotation:    Vector3f
}

impl From<(&Key3f, &Key3f)> for KeyFrame
{
    fn from(keys: (&Key3f, &Key3f)) -> Self
    {
        Self {
            t:           keys.0.t,
            translation: Vector3f::from(keys.0.value),
            rotation:    Vector3f::from(keys.1.value)
        }
    }
}

impl Default for KeyFrame
{
    fn default() -> Self
    {
        Self {
            t:           0.0,
            translation: Vector3f::default(),
            rotation:    Vector3f::default()
        }
    }
}
