use super::{StepAhead, StepAheadSave};
use crate::{
    engine::{
        drag_drop::DragDropManager,
        renderable::{RenderData, TryIntoRenderData}
    },
    type_aliases::{Isometry3f, Matrix4f, SampleTime}
};
use anyhow::Error;
use ggez::{
    graphics::{BackendSpec, GlBackendSpec},
    Context
};
use ncollide3d::query::RayCast;

#[derive(Default)]
pub struct StepAheadManager
{
    pub models:                Vec<StepAhead>,
    pub model_being_edited:    Option<usize>,
    pub keyframe_being_edited: Option<SampleTime>
}

impl StepAheadManager
{
    pub fn load(
        save_data: Vec<StepAheadSave>,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Result<Self, Error>
    {
        Ok(Self {
            models:                save_data
                .into_iter()
                .map(|step_ahead_save| {
                    StepAhead::load(step_ahead_save, factory).map(|step_ahead| step_ahead)
                })
                .collect::<Result<Vec<_>, _>>()?,
            model_being_edited:    Option::default(),
            keyframe_being_edited: Option::default()
        })
    }

    pub fn something_being_edited(&self) -> bool
    {
        self.model_being_edited
            .and_then(|m| self.keyframe_being_edited.map(|k| (m, k)))
            .is_some()
    }

    pub fn update_ffd_grid(
        &mut self,
        ctx: &mut Context,
        camera_iso: &Isometry3f,
        drag_drop_manager: &mut DragDropManager
    ) -> &mut Self
    {
        if self.something_being_edited() {
            if let Some(model) = self.models.get_mut(self.model_being_edited.unwrap()) {
                let sample_time = self.keyframe_being_edited.unwrap();
                let grid = model.ffd_grid_manager.get_mut(sample_time).unwrap();
                let bounds = grid
                    .iter()
                    .map(|(key, bound)| (key, Box::new(bound) as Box<dyn RayCast<f32>>));

                let new_position = drag_drop_manager.eval_mouse_state(ctx, &camera_iso, bounds);
                let dragged_item_idx = drag_drop_manager.grabbed_item_id();

                if let Some((pos, id)) = new_position.and_then(|n| dragged_item_idx.map(|d| (n, d)))
                {
                    grid.move_point(&id, pos).unwrap();
                }
            }
        }

        self
    }

    pub fn models_into_render_data(
        &self,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory,
        simulation_running: bool,
        physics_time: f32
    ) -> Vec<RenderData>
    {
        self.models
            .iter()
            .enumerate()
            .filter_map(|(idx, model)| {
                // if a FFD is being edited, we only show that model
                if simulation_running || !self.something_being_edited() {
                    Some(model)
                }
                else if self.something_being_edited() && Some(idx) == self.model_being_edited {
                    Some(model)
                }
                else {
                    None
                }
            })
            .map(|step_ahead| {
                let physics_time = if !simulation_running && self.something_being_edited() {
                    self.keyframe_being_edited.unwrap()
                }
                else {
                    physics_time
                };
                step_ahead.try_into_render_data(factory, physics_time)
            })
            .filter_map(|data| {
                if data.is_err() {
                    eprintln!("data = {:#?}", data);
                }
                data.ok()
            })
            .flatten()
            .map(|mut render_data| {
                if !simulation_running && self.something_being_edited() {
                    render_data.model_mat = Matrix4f::default();
                }
                render_data
            })
            .collect()
    }

    // Converts the ffd grid which is being edited to RenderData. Returns None if no
    // grid is being edited.
    pub fn grid_into_render_data(
        &mut self,
        simulation_running: bool,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Option<RenderData>
    {
        if self.something_being_edited() && !simulation_running {
            let model_being_edited = self.model_being_edited.unwrap();
            let sample_time = self.keyframe_being_edited.unwrap();

            self.models.get_mut(model_being_edited).and_then(|model| {
                TryIntoRenderData::try_into(
                    model.ffd_grid_manager.get_mut(sample_time).unwrap(),
                    factory
                )
                .ok()
            })
        }
        else {
            None
        }
    }
}

impl Into<Vec<StepAheadSave>> for &StepAheadManager
{
    fn into(self) -> Vec<StepAheadSave> { self.models.iter().map(Into::into).collect::<Vec<_>>() }
}
