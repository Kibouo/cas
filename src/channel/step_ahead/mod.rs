pub mod manager;

use super::{key_frame::KeyFrame, path::Path};
use crate::{
    engine::{
        ffd_grid::manager::{FFDGridManager, FFDGridManagerSave},
        mesh::Mesh,
        obj_parser::parse_obj,
        renderable::{RenderData, TryIntoRenderData}
    },
    type_aliases::Matrix4f
};
use anyhow::Error;
use decorum::R32;
use ggez::graphics::{BackendSpec, GlBackendSpec};
use std::{
    collections::HashSet,
    fmt::Debug,
    path::{Path as OSPath, PathBuf}
};

pub struct StepAhead
{
    file_path:        PathBuf,
    pub meshes:       Vec<Mesh>,
    path:             Path,
    pub model_name:   String,
    ffd_grid_manager: FFDGridManager
}

impl StepAhead
{
    pub fn add_keyframe(&mut self, keyframe: KeyFrame) -> &mut Self
    {
        self.path.add_keyframe(keyframe);
        let max_keyframe_idx = self.path.len() - 1;
        let max_sample_time = self.path.get(max_keyframe_idx).unwrap().t;
        self.ffd_grid_manager.last_sample_time = Some(max_sample_time);

        self
    }

    // old/new_sample_time are for when a keyframe got edited. This whole function
    // should be handled better but I'm sleepy and the deadline is near...
    // It's also bugged when you edit multiple items before confirming...
    pub fn update_path_from_keyframes<'a>(
        &mut self,
        path: impl Iterator<Item = &'a KeyFrame> + 'a
    ) -> &mut Self
    {
        let old_sample_times = self
            .path
            .keys()
            .map(|keyframe| R32::from(keyframe.t))
            .collect::<HashSet<_>>();
        self.path.update_from_keyframes(path);
        let new_sample_times = self
            .path
            .keys()
            .map(|keyframe| R32::from(keyframe.t))
            .collect::<HashSet<_>>();

        let (old_sample_time, new_sample_time) = {
            let old_diff = old_sample_times
                .difference(&new_sample_times)
                .map(|i| *i)
                .collect::<Vec<_>>();
            let new_diff = new_sample_times
                .difference(&old_sample_times)
                .collect::<Vec<_>>();

            if old_diff.len() == 0 && new_diff.len() == 0 {
                // nothing might have changed after all...
                (Some(R32::from(0.0)), Some(R32::from(0.0)))
            }
            else if old_diff.len() + new_diff.len() == 1 {
                // Something got added or removed
                (old_diff.get(0).map(|i| *i), new_diff.get(0).map(|i| **i))
            }
            else {
                (old_diff.get(0).map(|i| *i), new_diff.get(0).map(|i| **i))
            }
        };

        if self.path.len() > 0 {
            let max_keyframe_idx = self.path.len() - 1;
            self.ffd_grid_manager
                .update_keyframe(old_sample_time, new_sample_time);
            let max_sample_time = self.path.get(max_keyframe_idx).unwrap().t;
            self.ffd_grid_manager.last_sample_time = Some(max_sample_time);
        }

        self
    }

    pub fn path_keys<'a>(&'a self) -> impl Iterator<Item = KeyFrame> + 'a { self.path.keys() }

    pub fn load(
        save_data: StepAheadSave,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Result<Self, Error>
    {
        let mut step_ahead = Self::try_new(&save_data.file_path, factory)?;
        step_ahead.path = save_data.path;
        step_ahead
            .ffd_grid_manager
            .load_keyframes(save_data.ffd_grid_manager);

        Ok(step_ahead)
    }

    pub fn try_new<IP>(
        file_path: &IP,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Result<Self, Error>
    where
        IP: AsRef<OSPath> + Debug
    {
        let meshes = parse_obj(file_path, factory)?;
        let model_name = file_path
            .as_ref()
            .file_name()
            .map(|os_str| os_str.to_str())
            .flatten()
            .ok_or_else(|| anyhow!("No model file name for {:?}", file_path))?
            .to_owned();

        let ffd_grid_manager = FFDGridManager::from(&meshes);

        Ok(Self {
            file_path: file_path.as_ref().to_owned(),
            meshes,
            path: Path::default(),
            model_name,
            ffd_grid_manager
        })
    }

    pub fn try_into_render_data(
        &self,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory,
        physics_time: f32
    ) -> Result<Vec<RenderData>, anyhow::Error>
    {
        Ok(self
            .meshes
            .iter()
            .map(|mesh| {
                Mesh::new(
                    self.ffd_grid_manager
                        .apply(physics_time, mesh.vertices.clone()),
                    mesh.texture_with_sampler.clone()
                )
            })
            .map(|mesh| mesh.try_into(factory))
            .collect::<Result<Vec<_>, _>>()?
            .into_iter()
            .map(|mut render_data| {
                render_data.model_mat = self.model_mat_from_path(physics_time);

                render_data
            })
            .collect())
    }

    fn current_model_keyframe(&self, physics_time: f32) -> KeyFrame
    {
        self.path.clamped_sample(physics_time).unwrap_or_default()
    }

    fn model_mat_from_path(&self, physics_time: f32) -> Matrix4f
    {
        let keyframe = self.current_model_keyframe(physics_time);
        Matrix4f::new(keyframe.translation, keyframe.rotation)
    }
}

#[derive(Serialize, Deserialize)]
pub struct StepAheadSave
{
    file_path:        PathBuf,
    path:             Path,
    ffd_grid_manager: FFDGridManagerSave
}

impl From<&StepAhead> for StepAheadSave
{
    fn from(data: &StepAhead) -> Self
    {
        Self {
            file_path:        data.file_path.clone(),
            path:             data.path.clone(),
            ffd_grid_manager: (&data.ffd_grid_manager).into()
        }
    }
}
