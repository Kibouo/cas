use crate::{
    engine::{cube_vertex_data, renderable::TryIntoRenderData, ZFAR},
    image::load_img_as_2d_texture,
    type_aliases::{Matrix4f, TextureWithSampler, VerticesWithIndices}
};
use anyhow::{Context, Error};
use gfx::traits::FactoryExt;
use ggez::graphics;
use graphics::{BackendSpec, GlBackendSpec};
use image::ImageFormat;
use std::path::PathBuf;

pub struct SkyBox
{
    file_path:                PathBuf,
    pub model_mat:            Matrix4f,
    pub texture_with_sampler: Option<TextureWithSampler>
}

impl SkyBox
{
    pub fn load(
        save_data: SkyBoxSave,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Result<Self, Error>
    {
        let mut skybox = Self::default();

        skybox.model_mat = save_data.model_mat;
        if save_data.file_path.as_os_str() != "" {
            skybox.set_texture(save_data.file_path, factory)?;
        }

        Ok(skybox)
    }

    pub fn set_texture(
        &mut self,
        path: PathBuf,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Result<(), Error>
    {
        let image_format =
            ImageFormat::from_path(path.clone()).context("Unable to determine image format.")?;
        self.texture_with_sampler =
            Some(load_img_as_2d_texture(path.clone(), image_format, factory)?);
        self.file_path = path;

        Ok(())
    }
}

impl TryIntoRenderData for SkyBox
{
    fn vertex_data(
        &self,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Option<VerticesWithIndices>
    {
        let (vertex_data, index_data) = cube_vertex_data(ZFAR - 75.0, true, true);

        Some(factory.create_vertex_buffer_with_slice(&vertex_data, &index_data[..]))
    }

    fn texture_data(
        &self,
        _factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Option<TextureWithSampler>
    {
        self.texture_with_sampler.clone()
    }

    fn model_mat(&self) -> Matrix4f { self.model_mat.clone() }
}

impl Default for SkyBox
{
    fn default() -> Self
    {
        Self {
            file_path:            PathBuf::default(),
            model_mat:            Matrix4f::default(),
            texture_with_sampler: Option::default()
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct SkyBoxSave
{
    model_mat: Matrix4f,
    file_path: PathBuf
}

impl From<&SkyBox> for SkyBoxSave
{
    fn from(data: &SkyBox) -> Self
    {
        Self {
            model_mat: data.model_mat.clone(),
            file_path: data.file_path.clone()
        }
    }
}
