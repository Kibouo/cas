use imgui::ImStr;

pub mod key_frame;
pub mod path;
pub mod skybox;
pub mod step_ahead;

pub fn channels_ui_list() -> Vec<&'static ImStr>
{
    vec![
        im_str!("Camera path"),
        im_str!("Skybox"),
        im_str!("Step Ahead"),
    ]
}
