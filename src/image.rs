use crate::type_aliases::TextureWithSampler;
use anyhow::Error;
use gfx::{
    texture::{FilterMethod, SamplerInfo, WrapMode},
    Factory
};
use ggez::graphics::{BackendSpec, GlBackendSpec};
use image::{ImageFormat, RgbaImage};
use std::{fs::File, io::BufReader, path::PathBuf};

pub fn load_img_as_2d_texture(
    path: PathBuf,
    format: ImageFormat,
    factory: &mut <GlBackendSpec as BackendSpec>::Factory
) -> Result<TextureWithSampler, Error>
{
    let image = load_rgba_image(format, path)?;
    let (width, height) = image.dimensions();

    let (_, texture_view) = factory.create_texture_immutable_u8::<gfx::format::Srgba8>(
        gfx::texture::Kind::D2(width as u16, height as u16, gfx::texture::AaMode::Single),
        gfx::texture::Mipmap::Provided,
        &[image.into_raw().as_slice()]
    )?;
    let sampler_info = SamplerInfo::new(FilterMethod::Bilinear, WrapMode::Tile);

    Ok((texture_view, sampler_info))
}

fn load_rgba_image(format: ImageFormat, path: PathBuf) -> Result<RgbaImage, Error>
{
    let bufreader = BufReader::new(File::open(path)?);
    Ok(image::load(bufreader, format)?.to_rgba())
}
