use crate::engine::vertex;
use gfx::{
    handle::{Buffer, ShaderResourceView},
    texture::SamplerInfo,
    Slice
};
use ggez::graphics::{BackendSpec, GlBackendSpec};
use nalgebra::{Point3, Translation, UnitQuaternion};
use splines::Key;
use std::ops::{Add, AddAssign, Div, Mul, MulAssign, Sub, SubAssign};

#[derive(Deref, DerefMut, Debug, Clone, Serialize, Deserialize)]
pub struct Isometry3f(nalgebra::Isometry3<f32>);
#[derive(Deref, DerefMut, Debug, Clone, Default, PartialEq, Serialize, Deserialize)]
pub struct Vector3f(nalgebra::Vector3<f32>);
pub type HomogeneousMatrix = nalgebra::Matrix4<f32>;
pub type Translation3f = nalgebra::Translation3<f32>;
pub type UnitQuaternionFloat = nalgebra::UnitQuaternion<f32>;
pub type Perspective3f = nalgebra::Perspective3<f32>;
#[derive(Deref, DerefMut, Debug, Clone, Serialize, Deserialize)]
pub struct Matrix4f(nalgebra::Matrix4<f32>);

// ColorFormat and DepthFormat are used by ``ggez``
pub type ColorFormat = gfx::format::Srgba8;
pub type DepthFormat = gfx::format::DepthStencil;

pub type TextureWithSampler = (
    ShaderResourceView<<GlBackendSpec as BackendSpec>::Resources, [f32; 4]>,
    SamplerInfo
);
pub type VerticesWithIndices = (
    Buffer<<GlBackendSpec as BackendSpec>::Resources, vertex::Vertex>,
    Slice<<GlBackendSpec as BackendSpec>::Resources>
);

pub type SampleTime = f32;
pub type Spline3f = splines::Spline<SampleTime, nalgebra::Vector3<f32>>;
pub type Key3f = Key<SampleTime, nalgebra::Vector3<f32>>;

impl Isometry3f
{
    pub fn look_at_rh(eye: Vector3f, target: Vector3f) -> Self
    {
        Self(
            // Eye location, target location, up-vector
            nalgebra::Isometry3::<f32>::look_at_rh(
                &(*eye.inner()).into(),
                &(*target.inner()).into(),
                &nalgebra::Vector3::y_axis()
            )
        )
    }

    pub fn identity() -> Self { Self(nalgebra::Isometry3::<f32>::identity()) }

    pub fn translation(&self) -> Vector3f
    {
        let inv_rot = self.rotation.inverse();
        let translation = self.translation;

        Vector3f::from(-(inv_rot * translation).translation.vector)
    }
}

impl Matrix4f
{
    pub fn new(translation: Vector3f, rotation: Vector3f) -> Self
    {
        Self(
            Translation::from(*translation.inner()).to_homogeneous()
                * UnitQuaternion::from_euler_angles(rotation[0], rotation[1], rotation[2])
                    .to_homogeneous()
        )
    }
}

impl Default for Matrix4f
{
    fn default() -> Self
    {
        Self::from(nalgebra::Matrix4::new_translation(&Vector3f::new(
            0.0, 0.0, 0.0
        )))
    }
}

impl From<nalgebra::Matrix4<f32>> for Matrix4f
{
    fn from(m: nalgebra::Matrix4<f32>) -> Self { Self(m) }
}

impl Into<[[f32; 4]; 4]> for Matrix4f
{
    fn into(self) -> [[f32; 4]; 4] { self.0.into() }
}

impl Vector3f
{
    pub fn new(x: f32, y: f32, z: f32) -> Self { Self(nalgebra::Vector3::<f32>::new(x, y, z)) }

    pub fn inner(&self) -> &nalgebra::Vector3<f32> { &self.0 }
}

impl Into<nalgebra_glm::TVec3<f32>> for Vector3f
{
    fn into(self) -> nalgebra_glm::TVec3<f32> { nalgebra_glm::make_vec3(&self.0.data) }
}

impl Into<Point3<f32>> for Vector3f
{
    fn into(self) -> Point3<f32> { Point3::new(self.0.x, self.0.y, self.0.z) }
}

impl From<Point3<f32>> for Vector3f
{
    fn from(point: Point3<f32>) -> Self { Self::new(point.x, point.y, point.z) }
}

impl From<nalgebra_glm::TVec3<f32>> for Vector3f
{
    fn from(v: nalgebra_glm::TVec3<f32>) -> Self { Self::new(v.x, v.y, v.z) }
}

impl From<[f32; 4]> for Vector3f
{
    fn from(v: [f32; 4]) -> Self { Self::new(v[0] / v[3], v[1] / v[3], v[2] / v[3]) }
}

impl From<nalgebra::Vector4<f32>> for Vector3f
{
    fn from(v: nalgebra::Vector4<f32>) -> Self { Self::new(v[0] / v[3], v[1] / v[3], v[2] / v[3]) }
}

impl AddAssign for Vector3f
{
    fn add_assign(&mut self, rhs: Self) { self.0 += rhs.0; }
}

impl MulAssign<f32> for Vector3f
{
    fn mul_assign(&mut self, rhs: f32) { self.0 *= rhs; }
}

impl SubAssign for Vector3f
{
    fn sub_assign(&mut self, rhs: Self) { self.0 -= rhs.0; }
}

impl Add for Vector3f
{
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output { Self::from(self.inner() + rhs.inner()) }
}

impl Sub for Vector3f
{
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output { Self::from(self.inner() - rhs.inner()) }
}

impl Div<f32> for Vector3f
{
    type Output = Self;

    fn div(self, rhs: f32) -> Self::Output { Self::from(self.inner() / rhs) }
}

impl Mul<f32> for Vector3f
{
    type Output = Self;

    fn mul(self, rhs: f32) -> Self::Output { Self::from(self.inner() * rhs) }
}
