use crate::{
    engine::{renderable::TryIntoRenderData, vertex},
    type_aliases::{Matrix4f, TextureWithSampler, VerticesWithIndices}
};
use gfx::traits::FactoryExt;
use ggez::graphics::{BackendSpec, GlBackendSpec};

pub struct Mesh
{
    pub vertices:             Vec<vertex::Vertex>,
    pub texture_with_sampler: TextureWithSampler
}

impl Mesh
{
    pub fn new(vertices: Vec<vertex::Vertex>, texture_with_sampler: TextureWithSampler) -> Self
    {
        Self {
            vertices,
            texture_with_sampler
        }
    }
}

impl<'a> TryIntoRenderData for Mesh
{
    fn vertex_data(
        &self,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Option<VerticesWithIndices>
    {
        let indices = (0..self.vertices.len())
            .map(|i| i as u32)
            .collect::<Vec<_>>();

        Some(factory.create_vertex_buffer_with_slice(&self.vertices, &indices[..]))
    }

    fn texture_data(
        &self,
        _: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Option<TextureWithSampler>
    {
        Some(self.texture_with_sampler.clone())
    }

    fn model_mat(&self) -> Matrix4f { Matrix4f::default() }
}
