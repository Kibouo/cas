use super::{mesh::Mesh, Color};
use crate::{engine::vertex, image::load_img_as_2d_texture, type_aliases::TextureWithSampler};
use anyhow::{Context, Error};
use ggez::graphics::{BackendSpec, GlBackendSpec};
use image::ImageFormat;
use std::{convert::AsRef, fmt::Debug, path::Path, vec::Vec};
use tobj::{Material, Model};

pub fn parse_obj<IP>(
    obj_file_path: &IP,
    factory: &mut <GlBackendSpec as BackendSpec>::Factory
) -> Result<Vec<Mesh>, Error>
where
    IP: AsRef<Path> + Debug
{
    let (models, mats) = tobj::load_obj(obj_file_path, true)
        .with_context(|| format!("Loading of {:?} failed", obj_file_path))?;

    let diffuse_textures = load_diffuse_textures(obj_file_path, &mats, factory);
    let meshes =
        build_meshes(models, mats, &diffuse_textures).context("Failed to build meshes.")?;

    Ok(meshes)
}

fn load_diffuse_textures<IP>(
    obj_file_path: &IP,
    materials: &[Material],
    factory: &mut <GlBackendSpec as BackendSpec>::Factory
) -> Vec<TextureWithSampler>
where
    IP: AsRef<Path> + Debug
{
    materials
        .iter()
        .map(|mat| {
            let path = obj_file_path
                .as_ref()
                .parent()
                .unwrap()
                .join(&mat.diffuse_texture);
            ImageFormat::from_path(path.clone())
                .map(|image_format| {
                    load_img_as_2d_texture(path.into(), image_format, factory).unwrap_or_else(|e| {
                        eprintln!("Failed to load obj texture: {:?}", e);
                        crate::engine::colored_texture(factory, Color::Blue)
                    })
                })
                .unwrap_or_else(|e| panic!("Invalid image format: {:?}", e))
        })
        .collect()
}

fn build_meshes(
    models: Vec<Model>,
    materials: Vec<Material>,
    diffuse_textures: &[TextureWithSampler]
) -> Result<Vec<Mesh>, Error>
{
    models
        .into_iter()
        .map(|model| {
            let mesh = &model.mesh;
            let vertices = mesh
                .indices
                .iter()
                .map(|i| {
                    let i = *i as usize;

                    let position = [
                        mesh.positions[3 * i],
                        mesh.positions[3 * i + 1],
                        mesh.positions[3 * i + 2]
                    ];

                    let normal = if !mesh.normals.is_empty() {
                        [
                            mesh.normals[3 * i],
                            mesh.normals[3 * i + 1],
                            mesh.normals[3 * i + 2]
                        ]
                    }
                    else {
                        [0.0, 0.0, 0.0]
                    };

                    let texture_coords = if !mesh.texcoords.is_empty() {
                        [mesh.texcoords[2 * i], mesh.texcoords[2 * i + 1]]
                    }
                    else {
                        [0.0, 0.0]
                    };

                    let (color_diffuse, color_specular) = match mesh.material_id {
                        Some(i) => {
                            (materials[i].diffuse, [
                                materials[i].specular[0],
                                materials[i].specular[1],
                                materials[i].specular[2],
                                materials[i].shininess
                            ])
                        }
                        None => ([0.8, 0.8, 0.8], [0.15, 0.15, 0.15, 15.0])
                    };

                    vertex::Vertex::new(
                        position,
                        texture_coords,
                        normal,
                        color_diffuse,
                        color_specular
                    )
                })
                .collect();

            let texture_with_sampler = mesh
                .material_id
                .map(|i| diffuse_textures.get(i))
                .flatten()
                .ok_or_else(|| anyhow!("No diffuse texture for {}", model.name))?
                .clone();

            Ok(Mesh::new(vertices, texture_with_sampler))
        })
        .collect()
}
