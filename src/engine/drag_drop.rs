use super::ray_caster::RayCaster;
use crate::type_aliases::{Isometry3f, Vector3f};
use decorum::R32;
use ggez::{event::MouseButton, input::mouse, Context};
use ncollide3d::query::{RayCast, RayIntersection};

#[derive(Default)]
pub struct DragDropManager
{
    ray_caster:      RayCaster,
    grabbed_item:    Option<((i32, i32, i32), RayIntersection<f32>)>,
    prev_cursor_pos: (f32, f32)
}

impl DragDropManager
{
    // Returns position of dragged item, if anything has been dragged
    pub fn eval_mouse_state(
        &mut self,
        ctx: &mut Context,
        camera_iso: &Isometry3f,
        bounding_volumes: impl Iterator<Item = ((i32, i32, i32), Box<dyn RayCast<f32>>)>
    ) -> Option<Vector3f>
    {
        let mut new_position = None;
        self.ray_caster.set_ray(ctx, &camera_iso);

        if mouse::button_pressed(ctx, MouseButton::Left) && self.grabbed_item.is_none() {
            self.try_grab(bounding_volumes);
        }
        else if mouse::button_pressed(ctx, MouseButton::Left) && self.grabbed_item.is_some() {
            new_position = self.try_drag(ctx);
        }
        else if !mouse::button_pressed(ctx, MouseButton::Left) && self.grabbed_item.is_some() {
            self.grabbed_item = None;
        }

        self.prev_cursor_pos = {
            let pos = mouse::position(ctx);
            (pos.x, pos.y)
        };

        new_position
    }

    pub fn grabbed_item_id(&self) -> Option<(i32, i32, i32)>
    {
        self.grabbed_item.map(|item| item.0)
    }

    fn try_grab(
        &mut self,
        bounding_volumes: impl Iterator<Item = ((i32, i32, i32), Box<dyn RayCast<f32>>)>
    ) -> &mut Self
    {
        self.grabbed_item = bounding_volumes
            .filter_map(|(id, bound)| self.ray_caster.cast(bound).map(|intersect| (id, intersect)))
            .min_by_key(|(_, intersect)| R32::from(intersect.toi));

        self
    }

    fn try_drag<'a>(&mut self, ctx: &mut Context) -> Option<Vector3f>
    {
        let cursor_pos = mouse::position(ctx);
        let cursor_moved =
            cursor_pos.x != self.prev_cursor_pos.0 || cursor_pos.y != self.prev_cursor_pos.1;

        if cursor_moved {
            Some(
                self.ray_caster.calc_point_translation(
                    self.grabbed_item
                        .expect("Trying to drag while no item is grabbed.")
                        .1
                )
            )
        }
        else {
            None
        }
    }
}
