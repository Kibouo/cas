use crate::channel::key_frame::KeyFrame;
use imgui::{ImString, TextureId};
use std::path::PathBuf;

#[derive(Clone)]
pub enum ButtonAction
{
    Edit,
    Remove
}

#[derive(Clone, Default)]
pub struct KeyframeEditData
{
    pub keyframes:               Vec<KeyFrame>,
    pub keyframe_being_added:    KeyFrame,
    pub editing_timeline_action: Option<ButtonAction>,
    pub keyframe_being_edited:   (usize, KeyFrame),
    pub ffd_keyframe_edit_idx:   usize
}

#[derive(Clone)]
pub struct GuiData
{
    pub channel:         usize,
    pub camera_path:     CameraData,
    pub skybox:          SkyBoxData,
    pub step_ahead_data: StepAheadData,
    pub simulation_fps:  i32,
    pub load_save:       LoadSaveData
}

#[derive(Clone, Default)]
pub struct LoadSaveData
{
    pub failed_open_save_file: bool,
    pub load_file_path:        PathBuf
}

#[derive(Clone)]
pub struct CameraData
{
    pub keyframe_data: KeyframeEditData,
    pub show_path:     bool,
    pub speed_fn_img:  Vec<TextureId>
}

#[derive(Clone, Default)]
pub struct SkyBoxData
{
    pub img_path:    Option<PathBuf>,
    pub set_texture: bool
}

#[derive(Clone)]
pub struct StepAheadData
{
    pub obj_path:           Option<PathBuf>,
    pub load_obj:           bool,
    pub error_string:       String,
    pub model_names:        Vec<ImString>,
    pub keyframe_edit_data: Vec<KeyframeEditData>,
    pub selected_model:     usize
}

impl Default for StepAheadData
{
    fn default() -> Self
    {
        Self {
            obj_path:           Option::default(),
            load_obj:           bool::default(),
            error_string:       String::default(),
            model_names:        Vec::default(),
            keyframe_edit_data: vec![KeyframeEditData::default()],
            selected_model:     usize::default()
        }
    }
}

impl GuiData
{
    pub fn new(camera_path: CameraData) -> Self
    {
        Self {
            channel: usize::default(),
            camera_path,
            skybox: SkyBoxData::default(),
            step_ahead_data: StepAheadData::default(),
            simulation_fps: 60,
            load_save: LoadSaveData::default()
        }
    }
}

impl CameraData
{
    pub fn new(speed_fn_img: Vec<TextureId>) -> Self
    {
        Self {
            keyframe_data: KeyframeEditData::default(),
            show_path: true,
            speed_fn_img
        }
    }
}
