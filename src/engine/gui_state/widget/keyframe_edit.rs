use crate::{channel::key_frame::KeyFrame, engine::gui_state::Channel, type_aliases::Vector3f};
use imgui::{ImStr, ImString, Ui};
use std::fmt::Display;

pub fn keyframe_edit(ui: &Ui, title: &ImStr, keyframe: &mut KeyFrame, channel: &Channel) -> bool
{
    let mut t = keyframe.t;
    let mut location_xyz = [
        keyframe.translation.x,
        keyframe.translation.y,
        keyframe.translation.z
    ];
    let mut view_target_xyz = [
        keyframe.rotation.x,
        keyframe.rotation.y,
        keyframe.rotation.z
    ];
    let mut confirmed = false;

    ui.popup_modal(&title).always_auto_resize(true).build(|| {
        ui.input_float(im_str!("t"), &mut t).build();
        ui.input_float3(im_str!("Translation"), &mut location_xyz)
            .build();
        ui.input_float3(
            {
                match channel {
                    Channel::CameraPath => im_str!("View Target"),
                    Channel::StepAhead => im_str!("Rotation"),
                    _ => {
                        unreachable!(
                            "No keyframe editor should exist for channel {}",
                            Into::<&str>::into(*channel)
                        )
                    }
                }
            },
            &mut view_target_xyz
        )
        .build();

        if ui.button(im_str!("Confirm"), [0.0, 0.0]) {
            confirmed = true;
            ui.close_current_popup();
        }
        ui.same_line(0.0);
        if ui.button(im_str!("Cancel"), [0.0, 0.0]) {
            ui.close_current_popup();
        }
    });

    keyframe.t = t;
    keyframe.translation = Vector3f::new(location_xyz[0], location_xyz[1], location_xyz[2]);
    keyframe.rotation = Vector3f::new(view_target_xyz[0], view_target_xyz[1], view_target_xyz[2]);

    confirmed
}

pub fn keyframe_add_title(channel_str: impl Into<&'static str> + Display) -> ImString
{
    ImString::new(format!("Add {} Key Frame", channel_str))
}

pub fn keyframe_edit_title(channel_str: impl Into<&'static str> + Display) -> ImString
{
    ImString::new(format!("New {} Key Frame", channel_str))
}
