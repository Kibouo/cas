use std::path::{Path, PathBuf};

pub const IMAGE_FILE_EXT_FILTER: (&[&str], &str) =
    (&["*.jpg", "*.jpeg", "*.png"], "Supported image formats");
pub const OBJ_FILE_EXT_FILTER: (&[&str], &str) = (&["*.obj"], "Wavefront .obj file");
pub const SAVE_FILE_EXT_FILTER: (&[&str], &str) = (&["*.json"], "Editor save file (.json)");

pub fn browse_file(title: &str, base_path: &str, filter: Option<(&[&str], &str)>)
    -> Option<PathBuf>
{
    tinyfiledialogs::open_file_dialog(title, base_path, filter)
        .map(|file_path| Path::new(&file_path).to_owned())
}
