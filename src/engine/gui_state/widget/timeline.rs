use super::keyframe_edit::{keyframe_edit, keyframe_edit_title};
use crate::{
    channel::key_frame::KeyFrame,
    engine::gui_state::{
        gui_data::{ButtonAction, KeyframeEditData},
        Channel
    }
};
use imgui::{ImString, Ui};
use std::fmt::Display;

pub fn timeline_popup(
    ui: &Ui,
    channel_str: impl Into<&'static str> + Display + Copy,
    gui_keyframe_data: &mut KeyframeEditData,
    channel: &Channel
) -> bool
{
    let mut confirmed = false;

    ui.popup_modal(&timeline_title(channel_str))
        .always_auto_resize(true)
        .build(|| {
            let interaction =
                gui_keyframe_data
                    .keyframes
                    .iter()
                    .enumerate()
                    .find_map(|(i, keyframe)| {
                        if ui.small_button(&ImString::new(format!("Edit t = {}", keyframe.t))) {
                            ui.open_popup(&keyframe_edit_title(channel_str));
                            return Some((ButtonAction::Edit, i, keyframe.clone()))
                        }
                        ui.same_line(0.0);
                        if ui.small_button(&ImString::new(format!("Delete t = {}", keyframe.t))) {
                            return Some((ButtonAction::Remove, i, keyframe.clone()))
                        }

                        None
                    });
            if let Some((action, i, keyframe)) = interaction {
                gui_keyframe_data.editing_timeline_action = Some(action);
                gui_keyframe_data.keyframe_being_edited = (i, keyframe)
            }

            if let Some(action) = &gui_keyframe_data.editing_timeline_action {
                match action {
                    ButtonAction::Edit => {
                        let (i, mut keyframe) = gui_keyframe_data.keyframe_being_edited.clone();
                        if keyframe_edit(
                            &ui,
                            &keyframe_edit_title(channel_str),
                            &mut keyframe,
                            channel
                        ) {
                            // process data
                            gui_keyframe_data.keyframes[i] = keyframe;

                            // reset interaction state
                            gui_keyframe_data.editing_timeline_action = None;
                            gui_keyframe_data.keyframe_being_edited =
                                (usize::default(), KeyFrame::default());
                        }
                        else {
                            gui_keyframe_data.keyframe_being_edited = (i, keyframe);
                        }
                    }
                    ButtonAction::Remove => {
                        // process data
                        let (i, _) = &gui_keyframe_data.keyframe_being_edited;
                        gui_keyframe_data.keyframes.remove(*i);

                        // reset interaction state
                        gui_keyframe_data.editing_timeline_action = None;
                        gui_keyframe_data.keyframe_being_edited =
                            (usize::default(), KeyFrame::default());
                    }
                }
            }

            if ui.button(im_str!("Finish"), [0.0, 0.0]) {
                confirmed = true;
                ui.close_current_popup();
            }
        });

    confirmed
}

pub fn timeline_title(channel_str: impl Into<&'static str> + Display) -> ImString
{
    ImString::new(format!("{} Timeline", channel_str))
}
