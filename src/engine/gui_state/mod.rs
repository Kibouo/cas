mod gui_data;
mod widget;

use super::{
    app_state::{AppState, AppStateSave},
    input_state::{KeyboardState, MouseState}
};
use crate::{
    channel::{
        channels_ui_list,
        key_frame::KeyFrame,
        path::speed_fn::{speed_fn_graph_img_file_paths, speed_fn_ui_list},
        step_ahead::StepAhead
    },
    image::load_img_as_2d_texture
};
use anyhow::{Context as AnyhowContext, Error};
use gfx::{memory::Typed, Factory};
use ggez::{event::MouseButton, filesystem::resources_dir, graphics, Context as GgezContext};
use graphics::{BackendSpec, GlBackendSpec};
use gui_data::{CameraData, GuiData, KeyframeEditData};
use image::ImageFormat;
use imgui::{Context as ImguiContext, ImString};
use imgui_gfx_renderer::{Renderer, Shaders};
use num_traits::FromPrimitive;
use std::{io::Write, path::PathBuf};
use widget::{
    file_browser::{browse_file, IMAGE_FILE_EXT_FILTER, OBJ_FILE_EXT_FILTER, SAVE_FILE_EXT_FILTER},
    keyframe_edit::{keyframe_add_title, keyframe_edit},
    timeline::{timeline_popup, timeline_title}
};

const CONTROL_WIDTH: f32 = 200.0;
const SPEED_CURVE_IMG_DIMS: [f32; 2] = [120.0, 120.0];
const SAVE_FILE_PATH: &str = "./keyframe_savefile.json";

#[derive(FromPrimitive, Copy, Clone)]
pub enum Channel
{
    CameraPath = 0,
    Skybox     = 1,
    StepAhead  = 2
}

impl Into<&str> for Channel
{
    fn into(self) -> &'static str
    {
        match self {
            Channel::CameraPath => "Camera Path",
            Channel::Skybox => "Skybox",
            Channel::StepAhead => "Step Ahead"
        }
    }
}

pub struct GuiState
{
    context:  ImguiContext,
    renderer: Renderer<gfx::format::Rgba8, <GlBackendSpec as BackendSpec>::Resources>,
    pub data: GuiData
}

impl GuiState
{
    pub fn new(ctx: &mut GgezContext) -> Self
    {
        let resources_dir = resources_dir(ctx).to_owned();
        let (factory, gfx_device, ..) = graphics::gfx_objects(ctx);

        // Shaders
        let shaders = {
            let version = gfx_device.get_info().shading_language;
            if version.major >= 4 {
                Shaders::GlSl400
            }
            else if version.major >= 3 {
                Shaders::GlSl130
            }
            else {
                Shaders::GlSl110
            }
        };

        let mut context = ImguiContext::create();
        let mut renderer = Renderer::init(&mut context, &mut *factory, shaders)
            .expect("Failed to initialize GUI renderer.");

        let speed_fn_imgs = speed_fn_graph_img_file_paths(resources_dir)
            .iter()
            .map(|img_path| {
                let (texture_view, sampler_info) =
                    load_img_as_2d_texture(img_path.clone(), ImageFormat::Png, factory)
                        .expect("Failed to load speed fn graph image.");
                renderer
                    .textures()
                    .insert((texture_view, factory.create_sampler(sampler_info)))
            })
            .collect::<Vec<_>>();

        Self {
            context,
            renderer,
            data: GuiData::new(CameraData::new(speed_fn_imgs))
        }
    }

    pub fn render(
        &mut self,
        ctx: &mut GgezContext,
        mouse_state: &MouseState,
        keyboard_state: &mut KeyboardState,
        app_state: &mut AppState
    ) -> Result<(), Error>
    {
        self.pass_mouse_updates_to_imgui(mouse_state);
        self.pass_keyboard_updates_to_imgui(keyboard_state);

        let (draw_width, draw_height) = graphics::drawable_size(ctx);
        self.context.io_mut().display_size = [draw_width, draw_height];

        let mut gui_data = self.data.clone();
        let ui = self.context.frame();

        imgui::Window::new(im_str!("Menu"))
            .size([CONTROL_WIDTH, draw_height], imgui::Condition::FirstUseEver)
            .position([0.0, 0.0], imgui::Condition::FirstUseEver)
            .build(&ui, || {
                if gui_data.load_save.failed_open_save_file {
                    ui.text("Failed to open/parse save file.");
                }
                if ui.small_button(im_str!("Save Config")) {
                    let data = serde_json::to_string::<AppStateSave>(&(&*app_state).into())
                        .expect("Failed to convert AppState to JSON");
                    let mut save_file = std::fs::File::create(SAVE_FILE_PATH).unwrap();
                    save_file.write_all(data.as_bytes()).unwrap();
                }
                ui.same_line(0.0);
                if ui.small_button(im_str!("Load Config")) {
                    let load_file_path =
                        browse_file("Browse save file...", "./", Some(SAVE_FILE_EXT_FILTER));

                    if let Some(load_file_path) = load_file_path {
                        gui_data.load_save.load_file_path = load_file_path;
                    }
                }
                ui.separator();
                ui.spacing();

                /////////////////////////////////////////////////////////////////////////////
                if ui.small_button(im_str!("Start/Stop")) {
                    app_state.simulation_running = !app_state.simulation_running;
                }
                ui.same_line(0.0);
                if ui.small_button(im_str!("Reset")) {
                    app_state.reset_channels();
                }
                ui.input_int(im_str!("FPS"), &mut gui_data.simulation_fps)
                    .build();
                ui.separator();
                ui.spacing();

                /////////////////////////////////////////////////////////////////////////////
                imgui::ComboBox::new(im_str!("Channel")).build_simple_string(
                    &ui,
                    &mut gui_data.channel,
                    &channels_ui_list()
                );
                ui.spacing();
                ui.spacing();
                ui.spacing();

                /////////////////////////////////////////////////////////////////////////////
                let channel: Channel = (FromPrimitive::from_usize(gui_data.channel)
                    as Option<Channel>)
                    .expect("Invalid Channel index.");
                let channel_str = Into::<&str>::into(channel);
                match channel {
                    Channel::CameraPath => {
                        imgui::ComboBox::new(im_str!("SpeedFn")).build_simple_string(
                            &ui,
                            &mut app_state.camera_path.speed_fn_mut(),
                            &speed_fn_ui_list()
                        );
                        imgui::Image::new(
                            gui_data.camera_path.speed_fn_img
                                [*app_state.camera_path.speed_fn_mut()],
                            SPEED_CURVE_IMG_DIMS
                        )
                        .build(&ui);
                        ui.separator();
                        ui.spacing();

                        ////////////////////////////////////////////////////////////////
                        if ui.small_button(&ImString::new(format!("Add {} Key Frame", channel_str)))
                        {
                            ui.open_popup(&keyframe_add_title(channel_str));
                        }
                        if ui.small_button(&ImString::new(format!("Edit {} Timeline", channel_str)))
                        {
                            ui.open_popup(&timeline_title(channel_str));
                        }
                        if ui.small_button(&ImString::new(format!("Show/Hide {}", channel_str))) {
                            gui_data.camera_path.show_path = !gui_data.camera_path.show_path;
                        }

                        if keyframe_edit(
                            &ui,
                            &keyframe_add_title(channel_str),
                            &mut gui_data.camera_path.keyframe_data.keyframe_being_added,
                            &channel
                        ) {
                            // pass gui data to app
                            app_state.camera_path.add_keyframe(
                                gui_data
                                    .camera_path
                                    .keyframe_data
                                    .keyframe_being_added
                                    .clone()
                            );

                            // update gui state
                            gui_data.camera_path.keyframe_data.keyframes =
                                app_state.camera_path.keys().collect();

                            // reset interaction state
                            gui_data.camera_path.keyframe_data.keyframe_being_added =
                                KeyFrame::default();
                        }

                        if timeline_popup(
                            &ui,
                            channel_str,
                            &mut gui_data.camera_path.keyframe_data,
                            &channel
                        ) {
                            // pass gui data to app
                            app_state.camera_path.update_from_keyframes(
                                gui_data.camera_path.keyframe_data.keyframes.iter()
                            );

                            // update gui state
                            gui_data.camera_path.keyframe_data.keyframes =
                                app_state.camera_path.keys().collect();
                        }
                    }
                    Channel::Skybox => {
                        if ui.small_button(im_str!("Browse skybox image...")) {
                            gui_data.skybox.img_path = browse_file(
                                "Browse skybox image...",
                                "./",
                                Some(IMAGE_FILE_EXT_FILTER)
                            )
                        }
                        ui.same_line(0.0);
                        if ui.small_button(im_str!("Set skybox")) {
                            gui_data.skybox.set_texture = true;
                        }
                        ui.text(match gui_data.skybox.img_path.clone() {
                            None => ImString::new("No file selected."),
                            Some(path) => {
                                ImString::new(
                                    path.into_os_string()
                                        .into_string()
                                        .expect("Path contained no valid unicode.")
                                )
                            }
                        });
                    }
                    Channel::StepAhead => {
                        if ui.small_button(im_str!("Browse .obj file...")) {
                            gui_data.step_ahead_data.obj_path =
                                browse_file("Browse .obj file...", "./", Some(OBJ_FILE_EXT_FILTER));
                        }
                        ui.same_line(0.0);
                        if ui.small_button(im_str!("Load .obj")) {
                            gui_data.step_ahead_data.error_string = String::from("");
                            gui_data.step_ahead_data.load_obj = true;
                        }
                        ui.text(if gui_data.step_ahead_data.error_string != "" {
                            ImString::new(&gui_data.step_ahead_data.error_string)
                        }
                        else {
                            match gui_data.step_ahead_data.obj_path.clone() {
                                None => ImString::new("No file selected."),
                                Some(path) => {
                                    ImString::new(
                                        path.into_os_string()
                                            .into_string()
                                            .expect("Path contained no valid unicode.")
                                    )
                                }
                            }
                        });
                        ui.separator();
                        ui.spacing();

                        ////////////////////////////////////////////////////////////////////////////
                        imgui::ComboBox::new(im_str!("Select Model")).build_simple_string(
                            &ui,
                            &mut gui_data.step_ahead_data.selected_model,
                            &gui_data
                                .step_ahead_data
                                .model_names
                                .iter()
                                .collect::<Vec<_>>()[..]
                        );
                        ui.spacing();

                        if ui.small_button(&ImString::new(format!("Add {} Key Frame", channel_str)))
                        {
                            ui.open_popup(&keyframe_add_title(channel_str));
                        }
                        if ui.small_button(&ImString::new(format!("Edit {} Timeline", channel_str)))
                        {
                            ui.open_popup(&timeline_title(channel_str));
                        }

                        if keyframe_edit(
                            &ui,
                            &keyframe_add_title(channel_str),
                            &mut gui_data.step_ahead_data.keyframe_edit_data
                                [gui_data.step_ahead_data.selected_model]
                                .keyframe_being_added,
                            &channel
                        ) {
                            // pass gui data to app
                            if gui_data.step_ahead_data.keyframe_edit_data.len() > 0 {
                                app_state.step_ahead_manager.models
                                    [gui_data.step_ahead_data.selected_model]
                                    .add_keyframe(
                                        gui_data.step_ahead_data.keyframe_edit_data
                                            [gui_data.step_ahead_data.selected_model]
                                            .keyframe_being_added
                                            .clone()
                                    );

                                // update gui state
                                gui_data.step_ahead_data.keyframe_edit_data
                                    [gui_data.step_ahead_data.selected_model]
                                    .keyframes = app_state.step_ahead_manager.models
                                    [gui_data.step_ahead_data.selected_model]
                                    .path_keys()
                                    .collect();

                                // reset interaction state
                                gui_data.step_ahead_data.keyframe_edit_data
                                    [gui_data.step_ahead_data.selected_model]
                                    .keyframe_being_added = KeyFrame::default();
                            }
                        }

                        if timeline_popup(
                            &ui,
                            channel_str,
                            &mut gui_data.step_ahead_data.keyframe_edit_data
                                [gui_data.step_ahead_data.selected_model],
                            &channel
                        ) {
                            // pass gui data to app
                            if gui_data.step_ahead_data.keyframe_edit_data.len() > 0 {
                                app_state.step_ahead_manager.models
                                    [gui_data.step_ahead_data.selected_model]
                                    .update_path_from_keyframes(
                                        gui_data.step_ahead_data.keyframe_edit_data
                                            [gui_data.step_ahead_data.selected_model]
                                            .keyframes
                                            .iter()
                                    );

                                // update gui state
                                gui_data.step_ahead_data.keyframe_edit_data
                                    [gui_data.step_ahead_data.selected_model]
                                    .keyframes = app_state.step_ahead_manager.models
                                    [gui_data.step_ahead_data.selected_model]
                                    .path_keys()
                                    .collect();
                            }
                        }
                        ui.separator();
                        ui.spacing();

                        ////////////////////////////////////////////////////////////////////////////
                        if gui_data.step_ahead_data.keyframe_edit_data.len() > 0 {
                            ui.text(im_str!("Edit FFD"));
                            let mut options = vec![ImString::new("Don't edit")];
                            options.append(
                                &mut gui_data.step_ahead_data.keyframe_edit_data
                                    [gui_data.step_ahead_data.selected_model]
                                    .keyframes
                                    .iter()
                                    .map(|keyframe| ImString::new(format!("{}", keyframe.t)))
                                    .collect::<Vec<_>>()
                            );
                            imgui::ComboBox::new(im_str!("FFD Keyframe")).build_simple_string(
                                &ui,
                                &mut gui_data.step_ahead_data.keyframe_edit_data
                                    [gui_data.step_ahead_data.selected_model]
                                    .ffd_keyframe_edit_idx,
                                &options.iter().collect::<Vec<_>>()[..]
                            );

                            app_state.step_ahead_manager.model_being_edited =
                                Some(gui_data.step_ahead_data.selected_model);
                            app_state.step_ahead_manager.keyframe_being_edited = {
                                let model = &gui_data.step_ahead_data.keyframe_edit_data
                                    [gui_data.step_ahead_data.selected_model];

                                if model.ffd_keyframe_edit_idx == 0 {
                                    None
                                }
                                else {
                                    // `-1` to disregard the "don't edit" option
                                    model
                                        .keyframes
                                        .get(model.ffd_keyframe_edit_idx - 1)
                                        .map(|keyframe| keyframe.t)
                                }
                            };
                        }
                    }
                };
            });

        let draw_data = ui.render();

        let (factory, _, encoder, _, render_target) = graphics::gfx_objects(ctx);
        if &gui_data.load_save.load_file_path != &PathBuf::default() {
            match serde_json::from_reader(std::fs::File::open(&gui_data.load_save.load_file_path)?)
            {
                Ok(app_state_save) => {
                    *app_state = AppState::load(app_state_save, factory)?;

                    // Update GUI state
                    gui_data.camera_path.keyframe_data.keyframes = vec![];
                    gui_data.camera_path.keyframe_data.keyframes =
                        app_state.camera_path.keys().collect();

                    gui_data.step_ahead_data.keyframe_edit_data = vec![];
                    app_state
                        .step_ahead_manager
                        .models
                        .iter()
                        .enumerate()
                        .for_each(|(idx, model)| {
                            gui_data
                                .step_ahead_data
                                .keyframe_edit_data
                                .push(KeyframeEditData::default());
                            gui_data.step_ahead_data.keyframe_edit_data[idx].keyframes =
                                model.path_keys().collect()
                        });

                    gui_data.step_ahead_data.model_names = vec![];
                    gui_data.step_ahead_data.model_names = app_state
                        .step_ahead_manager
                        .models
                        .iter()
                        .map(|model| ImString::new(&model.model_name))
                        .collect::<Vec<_>>();

                    gui_data.load_save.failed_open_save_file = false;
                    gui_data.load_save.load_file_path = PathBuf::default();
                }
                Err(_) => gui_data.load_save.failed_open_save_file = true
            }
        }
        if gui_data.skybox.set_texture && gui_data.skybox.img_path.is_some() {
            let path = gui_data.skybox.img_path.as_ref().unwrap();
            app_state.skybox.set_texture(path.clone(), factory)?;
            gui_data.skybox.set_texture = false;
        }
        if gui_data.step_ahead_data.load_obj && gui_data.step_ahead_data.obj_path.is_some() {
            let path = gui_data.step_ahead_data.obj_path.as_ref().unwrap();

            match StepAhead::try_new(path, factory) {
                Ok(step_ahead_data) => {
                    app_state.step_ahead_manager.models.push(step_ahead_data);
                    gui_data.step_ahead_data.model_names = app_state
                        .step_ahead_manager
                        .models
                        .iter()
                        .map(|model| ImString::new(&model.model_name))
                        .collect::<Vec<_>>();
                }
                Err(e) => gui_data.step_ahead_data.error_string = format!("{:?}", e)
            }

            gui_data
                .step_ahead_data
                .keyframe_edit_data
                .push(KeyframeEditData::default());
            gui_data.step_ahead_data.load_obj = false;
        }

        self.renderer
            .render(
                &mut *factory,
                encoder,
                &mut gfx::handle::RenderTargetView::new(render_target.clone()),
                draw_data
            )
            .context("Failed to render GUI.")?;

        self.data = gui_data;
        Ok(())
    }

    fn pass_mouse_updates_to_imgui(&mut self, mouse_state: &MouseState)
    {
        self.context.io_mut().mouse_pos = [mouse_state.position.0, mouse_state.position.1];

        self.context.io_mut().mouse_down = [
            mouse_state.pressed_buttons.contains(&MouseButton::Left),
            mouse_state.pressed_buttons.contains(&MouseButton::Right),
            mouse_state.pressed_buttons.contains(&MouseButton::Middle),
            false,
            false
        ];
    }

    fn pass_keyboard_updates_to_imgui(&mut self, keyboard_state: &mut KeyboardState)
    {
        let imgui_io = self.context.io_mut();

        keyboard_state
            .pressed_keys
            .iter()
            .filter(|(_, used)| !**used)
            .for_each(|(key, _)| {
                if keyboard_state.pressed_mods.is_empty() {
                    match key {
                        ggez::event::KeyCode::Key1 => imgui_io.add_input_character('1'),
                        ggez::event::KeyCode::Key2 => imgui_io.add_input_character('2'),
                        ggez::event::KeyCode::Key3 => imgui_io.add_input_character('3'),
                        ggez::event::KeyCode::Key4 => imgui_io.add_input_character('4'),
                        ggez::event::KeyCode::Key5 => imgui_io.add_input_character('5'),
                        ggez::event::KeyCode::Key6 => imgui_io.add_input_character('6'),
                        ggez::event::KeyCode::Key7 => imgui_io.add_input_character('7'),
                        ggez::event::KeyCode::Key8 => imgui_io.add_input_character('8'),
                        ggez::event::KeyCode::Key9 => imgui_io.add_input_character('9'),
                        ggez::event::KeyCode::Key0 => imgui_io.add_input_character('0'),
                        ggez::event::KeyCode::Period => imgui_io.add_input_character('.'),
                        ggez::event::KeyCode::Subtract => imgui_io.add_input_character('-'),

                        // ggez::event::KeyCode::Delete => {}
                        // ggez::event::KeyCode::Back => {}
                        _ => {}
                    }
                }
            });

        keyboard_state.pressed_keys = keyboard_state
            .pressed_keys
            .iter()
            .map(|(key, _)| (*key, true))
            .collect();
    }
}
