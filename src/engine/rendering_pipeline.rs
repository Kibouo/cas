use super::vertex::Vertex;
use crate::type_aliases::*;

// the gfx macros require gfx, but clippy reminds us that module name use's are
// usually unneeded
#[allow(clippy::useless_attribute)]
#[allow(clippy::single_component_path_imports)]
use gfx;

gfx_defines! {
    // Buffers used by shaders
    constant Locals {
        model_mat: [[f32; 4]; 4] = "model_mat",
        view_mat: [[f32; 4]; 4] = "view_mat",
        proj_mat: [[f32; 4]; 4] = "proj_mat",
        eye: [f32;4] = "eye",
    }

    // The opengl object rendering pipeline
    pipeline pipe {
        vbuf: gfx::VertexBuffer<Vertex> = (),
        // per-object uniform buffers
        locals: gfx::ConstantBuffer<Locals> = "Locals",
        diffuse_texture: gfx::TextureSampler<[f32; 4]> = "diffuse_texture",
        out_color: gfx::RenderTarget<ColorFormat> = "out_color",
        out_depth: gfx::DepthTarget<DepthFormat> =
            gfx::preset::depth::LESS_EQUAL_WRITE,
    }
}
