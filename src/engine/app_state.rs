use super::drag_drop::DragDropManager;
use crate::{
    channel::{
        path::Path,
        skybox::{SkyBox, SkyBoxSave},
        step_ahead::{manager::StepAheadManager, StepAheadSave}
    },
    type_aliases::*
};
use anyhow::Error;
use ggez::{
    event::{KeyCode, KeyMods},
    graphics::{BackendSpec, GlBackendSpec},
    input::keyboard,
    Context
};
use nalgebra::Translation;
use std::time::Instant;

const MOVEMENT_SPEED: f32 = 0.15;
const FAST_FACTOR: f32 = 8.0;
const ROTATION_SPEED: f32 = 0.01;

pub struct AppState
{
    pub simulation_running: bool,
    pub frame:              u8,
    pub real_time:          Instant,
    pub physics_time:       f32,
    pub editor_camera:      Isometry3f,
    pub camera_path:        Path,
    pub skybox:             SkyBox,
    drag_drop_manager:      DragDropManager,
    pub step_ahead_manager: StepAheadManager
}

impl AppState
{
    pub fn load(
        save_data: AppStateSave,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Result<Self, Error>
    {
        Ok(Self {
            simulation_running: bool::default(),
            frame:              u8::default(),
            real_time:          Instant::now(),
            physics_time:       f32::default(),
            editor_camera:      save_data.editor_camera,
            camera_path:        save_data.camera_path,
            skybox:             SkyBox::load(save_data.skybox, factory)?,
            step_ahead_manager: StepAheadManager::load(save_data.step_ahead_models, factory)?,
            drag_drop_manager:  DragDropManager::default()
        })
    }

    // Make sure camera is centered in the middle of the skybox
    fn recenter_skybox(&mut self)
    {
        self.skybox.model_mat =
            Matrix4f::from(Translation::from(*self.eye_location().inner()).to_homogeneous());
    }

    pub fn eye_location(&self) -> Vector3f { self.camera_isometry3f().translation() }

    pub fn reset_channels(&mut self) -> &mut Self
    {
        self.physics_time = 0.0;
        self.simulation_running = false;

        self
    }

    pub fn physics_update(&mut self) -> &mut Self
    {
        let now = Instant::now();

        if self.simulation_running {
            self.physics_time += now.duration_since(self.real_time).as_secs_f32();
        }

        self.real_time = now;

        self
    }

    pub fn world_interaction_update(
        &mut self,
        ctx: &mut Context,
        simulation_running: bool
    ) -> &mut Self
    {
        if !simulation_running {
            self.step_ahead_manager.update_ffd_grid(
                ctx,
                &self.camera_isometry3f(),
                &mut self.drag_drop_manager
            );
        }

        self
    }

    pub fn camera_update(&mut self, ctx: &mut Context) -> &mut Self
    {
        fn translation(ctx: &mut Context) -> Translation3f
        {
            let mut translation = Vector3f::default();

            if keyboard::is_key_pressed(ctx, KeyCode::W) {
                translation += Vector3f::new(0.0, 0.0, MOVEMENT_SPEED);
            }
            else if keyboard::is_key_pressed(ctx, KeyCode::A) {
                translation += Vector3f::new(MOVEMENT_SPEED, 0.0, 0.0);
            }
            else if keyboard::is_key_pressed(ctx, KeyCode::S) {
                translation -= Vector3f::new(0.0, 0.0, MOVEMENT_SPEED);
            }
            else if keyboard::is_key_pressed(ctx, KeyCode::D) {
                translation -= Vector3f::new(MOVEMENT_SPEED, 0.0, 0.0);
            }
            else if keyboard::is_key_pressed(ctx, KeyCode::E) {
                translation -= Vector3f::new(0.0, MOVEMENT_SPEED, 0.0);
            }
            else if keyboard::is_key_pressed(ctx, KeyCode::Q) {
                translation += Vector3f::new(0.0, MOVEMENT_SPEED, 0.0);
            }

            if keyboard::is_mod_active(ctx, KeyMods::SHIFT) {
                translation *= FAST_FACTOR;
            }

            Translation::from(*translation.inner())
        }

        fn rotation(ctx: &mut Context) -> UnitQuaternionFloat
        {
            let mut euler = Vector3f::default();

            if keyboard::is_key_pressed(ctx, KeyCode::Up) {
                euler -= Vector3f::new(ROTATION_SPEED, 0.0, 0.0);
            }
            else if keyboard::is_key_pressed(ctx, KeyCode::PageDown) {
                euler += Vector3f::new(0.0, 0.0, ROTATION_SPEED);
            }
            else if keyboard::is_key_pressed(ctx, KeyCode::Down) {
                euler += Vector3f::new(ROTATION_SPEED, 0.0, 0.0);
            }
            else if keyboard::is_key_pressed(ctx, KeyCode::PageUp) {
                euler -= Vector3f::new(0.0, 0.0, ROTATION_SPEED);
            }
            else if keyboard::is_key_pressed(ctx, KeyCode::Left) {
                euler -= Vector3f::new(0.0, ROTATION_SPEED, 0.0);
            }
            else if keyboard::is_key_pressed(ctx, KeyCode::Right) {
                euler += Vector3f::new(0.0, ROTATION_SPEED, 0.0);
            }

            UnitQuaternionFloat::from_euler_angles(euler.x, euler.y, euler.z)
        }

        self.editor_camera.append_translation_mut(&translation(ctx));
        self.editor_camera.append_rotation_mut(&rotation(ctx));

        self.recenter_skybox();

        self
    }

    fn camera_isometry3f(&self) -> Isometry3f
    {
        if self.simulation_running && self.physics_time > 0.0 {
            self.camera_path
                .clamped_sample(self.physics_time)
                .map(|keyframe| Isometry3f::look_at_rh(keyframe.translation, keyframe.rotation))
                .unwrap_or_else(|e| {
                    if self.camera_path.len() > 0 {
                        eprintln!("{:?}", e);
                    }
                    self.editor_camera.clone()
                })
        }
        else {
            self.editor_camera.clone()
        }
    }

    pub fn camera_homogeneous(&self) -> HomogeneousMatrix
    {
        self.camera_isometry3f().to_homogeneous()
    }

    pub fn fps_counter(&mut self, ctx: &mut Context)
    {
        self.frame += 1;
        if (self.frame % 10) == 0 {
            println!("FPS: {}", ggez::timer::fps(ctx));
            self.frame = 0;
        }
    }
}

impl Default for AppState
{
    fn default() -> Self
    {
        Self {
            simulation_running: bool::default(),
            frame:              u8::default(),
            real_time:          Instant::now(),
            physics_time:       f32::default(),
            editor_camera:      Isometry3f::look_at_rh(
                Vector3f::new(0.0, 0.0, 40.0),
                Vector3f::default()
            ),
            camera_path:        Path::default(),
            skybox:             SkyBox::default(),
            drag_drop_manager:  DragDropManager::default(),
            step_ahead_manager: StepAheadManager::default()
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct AppStateSave
{
    editor_camera:     Isometry3f,
    camera_path:       Path,
    skybox:            SkyBoxSave,
    step_ahead_models: Vec<StepAheadSave>
}

impl From<&AppState> for AppStateSave
{
    fn from(data: &AppState) -> Self
    {
        Self {
            editor_camera:     data.editor_camera.clone(),
            camera_path:       data.camera_path.clone(),
            skybox:            (&data.skybox).into(),
            step_ahead_models: (&data.step_ahead_manager).into()
        }
    }
}
