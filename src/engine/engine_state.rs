use super::{
    app_state::AppState,
    gui_state::GuiState,
    input_state::{KeyboardState, MouseState},
    projection_matrix,
    renderable::{RenderData, TryIntoRenderData},
    rendering_pipeline::{pipe, Locals}
};
use gfx::{state::Rasterizer, traits::FactoryExt, Factory, Primitive};
use ggez::{
    event::{EventHandler, KeyCode, KeyMods, MouseButton},
    graphics,
    timer::sleep,
    Context,
    GameError,
    GameResult
};
use graphics::{BackendSpec, GlBackendSpec};
use std::time::Duration;

pub struct EngineState
{
    gui_state:            GuiState,
    app_state:            AppState,
    mouse_state:          MouseState,
    keyboard_state:       KeyboardState,
    // ``gfx`` state
    vertex_pipeline:      gfx::PipelineState<<GlBackendSpec as BackendSpec>::Resources, pipe::Meta>,
    dotted_line_pipeline: gfx::PipelineState<<GlBackendSpec as BackendSpec>::Resources, pipe::Meta>,
    dots_pipeline:        gfx::PipelineState<<GlBackendSpec as BackendSpec>::Resources, pipe::Meta>,
    cleared_frame:        bool
}

enum DrawPipeline
{
    Vertex,
    DottedLine,
    Dots
}

impl EventHandler for EngineState
{
    fn update(&mut self, ctx: &mut Context) -> GameResult
    {
        let simulation_running = self.app_state.simulation_running;
        self.app_state
            .camera_update(ctx)
            .physics_update()
            .world_interaction_update(ctx, simulation_running);
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult
    {
        if self.app_state.simulation_running {
            self.force_requested_fps(ctx);
        }

        self.render(ctx);
        self.gui_state
            .render(
                ctx,
                &self.mouse_state,
                &mut self.keyboard_state,
                &mut self.app_state
            )
            .map_err(|e| GameError::RenderError(e.to_string()))?;

        graphics::present(ctx)?;

        self.app_state.fps_counter(ctx);
        Ok(())
    }

    fn mouse_button_down_event(&mut self, _: &mut Context, button: MouseButton, _: f32, _: f32)
    {
        self.mouse_state.pressed_buttons.insert(button);
    }

    fn mouse_button_up_event(&mut self, _: &mut Context, button: MouseButton, _: f32, _: f32)
    {
        self.mouse_state.pressed_buttons.remove(&button);
    }

    fn mouse_motion_event(&mut self, _: &mut Context, x: f32, y: f32, _: f32, _: f32)
    {
        self.mouse_state.position = (x, y);
    }

    fn key_down_event(&mut self, _: &mut Context, keycode: KeyCode, keymod: KeyMods, _: bool)
    {
        self.keyboard_state.pressed_keys.insert(keycode, false);
        self.keyboard_state.pressed_mods = keymod;
    }

    fn key_up_event(&mut self, _: &mut Context, keycode: KeyCode, keymod: KeyMods)
    {
        self.keyboard_state.pressed_keys.remove(&keycode);
        self.keyboard_state.pressed_mods = keymod;
    }
}

impl EngineState
{
    pub fn new(ctx: &mut Context) -> Self
    {
        let (factory, ..) = graphics::gfx_objects(ctx);

        // Shaders.
        let vs = include_str!("../../assets/vertex_150.glslv");
        let fs = include_str!("../../assets/fragment_150.glslf");

        // Create pipeline state objects
        let program = factory
            .link_program(vs.as_bytes(), fs.as_bytes())
            .expect("Failed to link shaders into program.");
        let mut rasterizer = Rasterizer::new_fill().with_cull_back();

        let vertex_pipeline = factory
            .create_pipeline_from_program(
                &program,
                Primitive::TriangleList,
                rasterizer,
                pipe::new()
            )
            .expect("Failed to create vertex pipeline.");

        rasterizer.method = gfx::state::RasterMethod::Line(2);
        let dotted_line_pipeline = factory
            .create_pipeline_from_program(&program, Primitive::LineList, rasterizer, pipe::new())
            .expect("Failed to create line pipeline.");

        let dots_pipeline = factory
            .create_pipeline_from_program(&program, Primitive::PointList, rasterizer, pipe::new())
            .expect("Failed to create line pipeline.");

        EngineState {
            gui_state: GuiState::new(ctx),
            app_state: AppState::default(),
            mouse_state: MouseState::default(),
            keyboard_state: KeyboardState::default(),
            vertex_pipeline,
            dotted_line_pipeline,
            dots_pipeline,
            cleared_frame: bool::default()
        }
    }

    fn force_requested_fps(&self, ctx: &Context)
    {
        let actual_frame_duration = ggez::timer::delta(ctx);
        let requested_frame_duration: Duration =
            Duration::new(1, 0) / (self.gui_state.data.simulation_fps as u32);

        let sleep_time = if requested_frame_duration < actual_frame_duration {
            Duration::new(0, 0)
        }
        else {
            requested_frame_duration - actual_frame_duration
        };

        sleep(sleep_time);
    }

    fn render(&mut self, ctx: &mut Context)
    {
        let (factory, device, encoder, depth_view, color_view) = graphics::gfx_objects(ctx);

        let mut render_data = self.app_state.step_ahead_manager.models_into_render_data(
            factory,
            self.app_state.simulation_running,
            self.app_state.physics_time
        );
        if self.app_state.skybox.texture_with_sampler.is_some() {
            render_data.append(&mut vec![TryIntoRenderData::try_into(
                &self.app_state.skybox,
                factory
            )
            .expect("Failed to load skybox")]);
        }

        // meshes
        render_data.into_iter().for_each(|render_data| {
            self.draw_vertex_obj(
                render_data,
                DrawPipeline::Vertex,
                factory,
                encoder,
                depth_view.clone(),
                color_view.clone()
            )
        });

        // camera path
        if self.gui_state.data.camera_path.show_path {
            let _ = TryIntoRenderData::try_into(&self.app_state.camera_path, factory).map(
                |render_data| {
                    self.draw_vertex_obj(
                        render_data,
                        DrawPipeline::DottedLine,
                        factory,
                        encoder,
                        depth_view.clone(),
                        color_view.clone()
                    )
                }
            );
        }

        // ffd grid
        if let Some(render_data) = self
            .app_state
            .step_ahead_manager
            .grid_into_render_data(self.app_state.simulation_running, factory)
        {
            self.draw_vertex_obj(
                render_data,
                DrawPipeline::Dots,
                factory,
                encoder,
                depth_view,
                color_view
            )
        }

        self.cleared_frame = false;
        encoder.flush(device);
    }

    fn draw_vertex_obj(
        &mut self,
        render_data: RenderData,
        pipeline_to_use: DrawPipeline,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory,
        encoder: &mut gfx::Encoder<
            <GlBackendSpec as BackendSpec>::Resources,
            <GlBackendSpec as BackendSpec>::CommandBuffer
        >,
        depth_view: gfx::handle::RawDepthStencilView<<GlBackendSpec as BackendSpec>::Resources>,
        color_view: gfx::handle::RawRenderTargetView<<GlBackendSpec as BackendSpec>::Resources>
    )
    {
        let eye: [f32; 3] = (*self.app_state.eye_location().inner()).into();
        let locals = Locals {
            model_mat: render_data.model_mat.into(),
            view_mat:  self.app_state.camera_homogeneous().into(),
            proj_mat:  projection_matrix().to_homogeneous().into(),
            eye:       [eye[0], eye[1], eye[2], 1.0]
        };

        let gpu_data = pipe::Data {
            vbuf:            render_data.vertices_with_indices.0,
            locals:          factory.create_constant_buffer(1),
            diffuse_texture: (
                render_data.texture_with_sampler.0,
                factory.create_sampler(render_data.texture_with_sampler.1)
            ),
            // We use the (undocumented-but-useful) gfx::memory::Typed here
            // to convert ggez's raw render and depth buffers into ones with
            // compile-time type information.
            out_color:       gfx::memory::Typed::new(color_view),
            out_depth:       gfx::memory::Typed::new(depth_view)
        };

        if !self.cleared_frame {
            encoder.clear(&gpu_data.out_color, [0.0, 0.0, 0.0, 1.0]);
            encoder.clear_depth(&gpu_data.out_depth, 1.0);
            self.cleared_frame = true;
        }
        encoder.update_constant_buffer(&gpu_data.locals, &locals);
        encoder.draw(
            &render_data.vertices_with_indices.1,
            {
                match pipeline_to_use {
                    DrawPipeline::Vertex => &self.vertex_pipeline,
                    DrawPipeline::DottedLine => &self.dotted_line_pipeline,
                    DrawPipeline::Dots => &self.dots_pipeline
                }
            },
            &gpu_data
        );
    }
}
