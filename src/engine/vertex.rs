gfx_defines! {
    vertex Vertex {
        pos: [f32; 4] = "a_Pos",
        normal: [f32; 4] = "a_Normal",
        color_diffuse: [f32; 4] = "a_ColorDiffuse",
        color_specular: [f32; 4] = "a_ColorSpecular",
        tex_coord: [f32; 2] = "a_TexCoord",
    }
}

impl Vertex
{
    pub fn new(
        p: [f32; 3],
        t: [f32; 2],
        n: [f32; 3],
        color_diffuse: [f32; 3],
        color_specular: [f32; 4]
    ) -> Vertex
    {
        Vertex {
            pos: [p[0], p[1], p[2], 1.0],
            normal: [n[0], n[1], n[2], 0.0],
            tex_coord: t,
            color_diffuse: [color_diffuse[0], color_diffuse[1], color_diffuse[2], 1.0],
            color_specular
        }
    }

    pub fn new_no_color(p: [f32; 3], t: [f32; 2]) -> Vertex
    {
        Vertex::new(p, t, [0.0, 0.0, 0.0], [1.0, 1.0, 1.0], [1.0, 1.0, 1.0, 1.0])
    }
}

impl From<nalgebra::Vector3<f32>> for Vertex
{
    fn from(vec: nalgebra::Vector3<f32>) -> Self
    {
        Vertex::new(
            [vec.x, vec.y, vec.z],
            [0.0, 0.0],
            [0.0, 0.0, 0.0],
            [1.0, 1.0, 1.0],
            [1.0, 1.0, 1.0, 1.0]
        )
    }
}

impl From<nalgebra::Point3<f32>> for Vertex
{
    fn from(point: nalgebra::Point3<f32>) -> Self
    {
        Self::from(nalgebra::Vector3::<f32>::new(point.x, point.y, point.z))
    }
}
