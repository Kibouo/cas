mod app_state;
pub mod drag_drop;
pub mod engine_state;
pub mod ffd_grid;
mod gui_state;
mod input_state;
pub mod mesh;
pub mod obj_parser;
mod ray_caster;
pub mod renderable;
pub mod rendering_pipeline;
pub mod vertex;

use crate::type_aliases::{Perspective3f, TextureWithSampler};
use gfx::{texture, Factory};
use ggez::graphics;
use graphics::{BackendSpec, GlBackendSpec};
use vertex::Vertex;

pub const ZFAR: f32 = 175.0;

const CUBEMAP_SIDE_OFFSETS: [(usize, usize); 6] = [(1, 0), (1, 2), (2, 1), (0, 1), (1, 1), (3, 1)];

pub fn cube_vertex_data(
    factor: f32,
    inside_out: bool,
    cubemap_texture: bool // whether the texture coords are that of a cubemap
) -> (Vec<Vertex>, Vec<u32>)
{
    // Cube geometry
    let vertex_data = vec![
        // top (0, 1.0, 0)
        ([1.0, 1.0, -1.0], [1.0, 0.0]),
        ([-1.0, 1.0, -1.0], [0.0, 0.0]),
        ([-1.0, 1.0, 1.0], [0.0, 1.0]),
        ([1.0, 1.0, 1.0], [1.0, 1.0]),
        // bottom (0, -1.0, 0)
        ([1.0, -1.0, 1.0], [1.0, 0.0]),
        ([-1.0, -1.0, 1.0], [0.0, 0.0]),
        ([-1.0, -1.0, -1.0], [0.0, 1.0]),
        ([1.0, -1.0, -1.0], [1.0, 1.0]),
        // right (1.0, 0, 0)
        ([1.0, -1.0, -1.0], [1.0, 1.0]),
        ([1.0, 1.0, -1.0], [1.0, 0.0]),
        ([1.0, 1.0, 1.0], [0.0, 0.0]),
        ([1.0, -1.0, 1.0], [0.0, 1.0]),
        // left (-1.0, 0, 0)
        ([-1.0, -1.0, 1.0], [1.0, 1.0]),
        ([-1.0, 1.0, 1.0], [1.0, 0.0]),
        ([-1.0, 1.0, -1.0], [0.0, 0.0]),
        ([-1.0, -1.0, -1.0], [0.0, 1.0]),
        // front (0, 0, 1)
        ([-1.0, -1.0, 1.0], [0.0, 1.0]),
        ([1.0, -1.0, 1.0], [1.0, 1.0]),
        ([1.0, 1.0, 1.0], [1.0, 0.0]),
        ([-1.0, 1.0, 1.0], [0.0, 0.0]),
        // back (0, 0, -1)
        ([-1.0, 1.0, -1.0], [1.0, 0.0]),
        ([1.0, 1.0, -1.0], [0.0, 0.0]),
        ([1.0, -1.0, -1.0], [0.0, 1.0]),
        ([-1.0, -1.0, -1.0], [1.0, 1.0]),
    ]
    .into_iter()
    .map(|(pos, tex)| Vertex::new_no_color(pos,tex))
    // scale the cube
    .map(|mut vertex| {
        vertex.pos[0] *= factor;
        vertex.pos[1] *= factor;
        vertex.pos[2] *= factor;
        vertex
    })
    .collect::<Vec<_>>()
    // scale texture coordinates in case of a cubemap
    .chunks_mut(4)
    .enumerate()
    .map(|(side_idx, vertices)| {
        if cubemap_texture {
            let (x_idx, y_idx) = CUBEMAP_SIDE_OFFSETS[side_idx];
            vertices.into_iter().map(|mut vertex|
            {
                vertex.tex_coord[0] = (x_idx as f32 +
vertex.tex_coord[0])/4.0;                 vertex.tex_coord[1] = (y_idx as f32
+ vertex.tex_coord[1])/3.0;                 *vertex
            }).collect::<Vec<Vertex>>()

        }else{vertices.into_iter().map(|v|*v).collect::<Vec<Vertex>>()}
    })
    .flatten()
    .collect::<Vec<_>>();

    let mut index_data: Vec<u32> = vec![
        0, 1, 2, 2, 3, 0, // top
        4, 5, 6, 6, 7, 4, // bottom
        8, 9, 10, 10, 11, 8, // right
        12, 13, 14, 14, 15, 12, // left
        16, 17, 18, 18, 19, 16, // front
        20, 21, 22, 22, 23, 20, // back
    ];

    let index_data = if inside_out {
        index_data.reverse();
        index_data
    }
    else {
        index_data
    };

    (vertex_data, index_data)
}

pub enum Color
{
    Red,
    Blue,
    Green
}

pub fn colored_texture(
    factory: &mut <GlBackendSpec as BackendSpec>::Factory,
    color: Color
) -> TextureWithSampler
{
    // Create 1-pixel blue texture & sampler
    let texel = {
        match color {
            Color::Red => [[0xD1, 0x21, 0x44, 0x00]],
            Color::Blue => [[0x20, 0xA0, 0xC0, 0x00]],
            Color::Green => [[0x05, 0xFC, 0x53, 0x00]]
        }
    };
    let (_, texture_view) = factory
        .create_texture_immutable::<gfx::format::Rgba8>(
            texture::Kind::D2(1, 1, texture::AaMode::Single),
            texture::Mipmap::Provided,
            &[&texel]
        )
        .expect("Failed to create texture.");
    let sampler_info =
        texture::SamplerInfo::new(texture::FilterMethod::Bilinear, texture::WrapMode::Clamp);

    (texture_view, sampler_info)
}

pub fn projection_matrix() -> Perspective3f
{
    // Aspect ratio, FOV, znear, zfar
    nalgebra::Perspective3::new(4.0 / 3.0, std::f32::consts::PI / 4.0, 0.1, ZFAR)
}
