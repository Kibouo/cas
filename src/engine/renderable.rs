use crate::type_aliases::*;
use anyhow::Error;
use ggez::graphics;
use graphics::{BackendSpec, GlBackendSpec};

pub trait TryIntoRenderData
{
    fn try_into(
        &self,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Result<RenderData, Error>
    {
        let vertices_with_indices = self
            .vertex_data(factory)
            .ok_or_else(|| anyhow!("No vertex data"))?;
        let texture_with_sampler = self
            .texture_data(factory)
            .ok_or_else(|| anyhow!("No texture/sampler data"))?;
        let model_mat = self.model_mat();

        Ok(RenderData {
            texture_with_sampler,
            vertices_with_indices,
            model_mat
        })
    }

    fn vertex_data(
        &self,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Option<VerticesWithIndices>;

    fn texture_data(
        &self,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Option<TextureWithSampler>;

    fn model_mat(&self) -> Matrix4f;
}

#[derive(Debug)]
pub struct RenderData
{
    pub texture_with_sampler:  TextureWithSampler,
    pub vertices_with_indices: VerticesWithIndices,
    pub model_mat:             Matrix4f
}
