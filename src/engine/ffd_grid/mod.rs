pub mod manager;
mod point_collection_3d;

use super::{mesh::Mesh, renderable::TryIntoRenderData, vertex, Color};
use crate::{
    engine::colored_texture,
    type_aliases::{Matrix4f, TextureWithSampler, Vector3f, VerticesWithIndices}
};
use anyhow::Error;
use decorum::R32;
use gfx::traits::FactoryExt;
use ggez::graphics::{BackendSpec, GlBackendSpec};
use nalgebra::Point3;
use ncollide3d::bounding_volume::BoundingSphere;
use point_collection_3d::PointCollection3D;

const BOUNDING_SPHERE_RADIUS: f32 = 0.15;

#[derive(Clone)]
pub struct FFDGrid
{
    grid_points: PointCollection3D
}

impl FFDGrid
{
    pub fn linear_interpolate_with(&self, other: &Self, factor: f32) -> Self
    {
        Self {
            grid_points: self
                .grid_points
                .linear_interpolate_with(&other.grid_points, factor)
        }
    }

    pub fn load_and_apply_changes(mut self, save_data: &FFDGridSave) -> Self
    {
        save_data.grid_points.iter().for_each(|(id, bound)| {
            if let Some(point) = self.grid_points.get_mut(id) {
                *point = Some(BoundingSphere::<f32>::new(
                    Point3::new(bound.x, bound.y, bound.z),
                    BOUNDING_SPHERE_RADIUS
                ));
            }
        });

        self
    }

    pub fn move_point(&mut self, id: &(i32, i32, i32), location: Vector3f) -> Result<(), Error>
    {
        if let Some(point) = self.grid_points.get_mut(id) {
            *point = Some(BoundingSphere::<f32>::new(
                Point3::new(location.x, location.y, location.z),
                BOUNDING_SPHERE_RADIUS
            ));
            Ok(())
        }
        else {
            Err(anyhow!("No point with id {:?} to be moved.", id))
        }
    }

    fn new(min_corner: Vector3f, max_corner: Vector3f) -> Self
    {
        let min_corner = (
            min_corner.x.floor() as i32,
            min_corner.y.floor() as i32,
            min_corner.z.floor() as i32
        );
        let max_corner = (
            max_corner.x.ceil() as i32,
            max_corner.y.ceil() as i32,
            max_corner.z.ceil() as i32
        );

        Self {
            grid_points: PointCollection3D::new(min_corner, max_corner)
        }
    }

    pub fn iter(&self) -> impl Iterator<Item = ((i32, i32, i32), BoundingSphere<f32>)> + '_
    {
        self.grid_points.iter().map(|(id, bound)| {
            let bound = bound.clone().unwrap_or_else(|| {
                BoundingSphere::<f32>::new(
                    Point3::new(id.0 as f32, id.1 as f32, id.2 as f32),
                    BOUNDING_SPHERE_RADIUS
                )
            });
            (id, bound)
        })
    }

    pub fn containing_cube_altered(&self, vertex: &vertex::Vertex) -> bool
    {
        Self::calc_corners_containing_cube(vertex)
            .into_iter()
            .fold(false, |mut altered, corner| {
                altered |= self
                    .grid_points
                    .get(&corner)
                    .expect(
                        "(x,y,z) was build from the vertices which the original grid was also \
                         built from. This should thus always return an item."
                    )
                    .is_some();
                altered
            })
    }

    fn calc_corners_containing_cube(vertex: &vertex::Vertex) -> Vec<(i32, i32, i32)>
    {
        let min_corner = (
            vertex.pos[0].floor() as i32,
            vertex.pos[1].floor() as i32,
            vertex.pos[2].floor() as i32
        );
        let max_corner = (
            vertex.pos[0].ceil() as i32,
            vertex.pos[1].ceil() as i32,
            vertex.pos[2].ceil() as i32
        );

        let mut cube_corners = Vec::with_capacity(8);
        for x in [min_corner.0, max_corner.0].iter() {
            for y in [min_corner.1, max_corner.1].iter() {
                for z in [min_corner.2, max_corner.2].iter() {
                    cube_corners.push((*x, *y, *z));
                }
            }
        }

        cube_corners
    }

    pub fn apply(&self, mut vertex: vertex::Vertex) -> vertex::Vertex
    {
        let orig_cube = Self::calc_corners_containing_cube(&vertex);
        let mod_cube = orig_cube
            .iter()
            .map(|corner| {
                self.grid_points
                    .get(corner)
                    .expect(
                        "(x,y,z) was build from the vertices which the original grid was also \
                         built from. This should thus always return an item."
                    )
                    .as_ref()
                    .map(|bound| Vector3f::from(*bound.center()))
                    .unwrap_or_else(|| {
                        Vector3f::new(corner.0 as f32, corner.1 as f32, corner.2 as f32)
                    })
            })
            .collect::<Vec<_>>();

        // linear interpolation
        let min_corner = &orig_cube[0];
        let x_factor = vertex.pos[0] - (min_corner.0 as f32);
        let y_factor = vertex.pos[1] - (min_corner.1 as f32);
        let z_factor = vertex.pos[2] - (min_corner.2 as f32);

        // along x
        let c00 = mod_cube[0].clone() * (1.0 - x_factor) + mod_cube[4].clone() * x_factor;
        let c01 = mod_cube[2].clone() * (1.0 - x_factor) + mod_cube[6].clone() * x_factor;
        let c10 = mod_cube[1].clone() * (1.0 - x_factor) + mod_cube[5].clone() * x_factor;
        let c11 = mod_cube[3].clone() * (1.0 - x_factor) + mod_cube[7].clone() * x_factor;

        // along y
        let c0 = c00 * (1.0 - y_factor) + c10 * y_factor;
        let c1 = c01 * (1.0 - y_factor) + c11 * y_factor;

        // along z
        let c: Vector3f = c0 * (1.0 - z_factor) + c1 * z_factor;
        vertex.pos = [c.x, c.y, c.z, 1.0];

        vertex
    }
}

impl From<&Vec<Mesh>> for FFDGrid
{
    fn from(meshes: &Vec<Mesh>) -> Self
    {
        let mut min_pos = Vector3f::from(meshes[0].vertices[0].pos);
        let mut max_pos = min_pos.clone();

        meshes.iter().for_each(|mesh| {
            mesh.vertices.iter().skip(1).for_each(|vertex| {
                let position = Vector3f::from(vertex.pos);

                min_pos[0] = R32::from(min_pos[0]).min(position[0].into()).into();
                min_pos[1] = R32::from(min_pos[1]).min(position[1].into()).into();
                min_pos[2] = R32::from(min_pos[2]).min(position[2].into()).into();

                max_pos[0] = R32::from(max_pos[0]).max(position[0].into()).into();
                max_pos[1] = R32::from(max_pos[1]).max(position[1].into()).into();
                max_pos[2] = R32::from(max_pos[2]).max(position[2].into()).into();
            });
        });

        FFDGrid::new(min_pos, max_pos)
    }
}

impl<'a> TryIntoRenderData for FFDGrid
{
    fn vertex_data(
        &self,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Option<VerticesWithIndices>
    {
        let indices = (0..self.grid_points.len())
            .map(|i| i as u32)
            .collect::<Vec<_>>();
        let vertices = self
            .grid_points
            .iter()
            .map(|(id, bound)| {
                if let Some(bound) = bound {
                    (*bound.center()).into()
                }
                else {
                    (*Vector3f::new(id.0 as f32, id.1 as f32, id.2 as f32).inner()).into()
                }
            })
            .collect::<Vec<vertex::Vertex>>();

        Some(factory.create_vertex_buffer_with_slice(&vertices, &indices[..]))
    }

    fn texture_data(
        &self,
        factory: &mut <GlBackendSpec as BackendSpec>::Factory
    ) -> Option<TextureWithSampler>
    {
        Some(colored_texture(factory, Color::Red))
    }

    fn model_mat(&self) -> Matrix4f { Matrix4f::default() }
}

#[derive(Serialize, Deserialize)]
pub struct FFDGridSave
{
    grid_points: Vec<((i32, i32, i32), Vector3f)>
}

impl From<&FFDGrid> for FFDGridSave
{
    fn from(data: &FFDGrid) -> Self
    {
        Self {
            grid_points: data
                .grid_points
                .iter()
                .filter_map(|(id, bound)| {
                    bound
                        .as_ref()
                        .map(|bound| (id, Vector3f::from(*bound.center())))
                })
                .collect()
        }
    }
}
