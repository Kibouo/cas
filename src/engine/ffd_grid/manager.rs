use super::FFDGridSave;
use crate::{
    engine::{ffd_grid::FFDGrid, mesh::Mesh, vertex},
    type_aliases::SampleTime
};
use anyhow::Error;
use decorum::R32;
use std::collections::HashMap;

type FFDGridKeyFrame<'a> = (SampleTime, &'a FFDGrid);

pub struct FFDGridManager
{
    default:              FFDGrid,
    keyframes:            HashMap<R32, FFDGrid>,
    pub last_sample_time: Option<SampleTime>
}

impl FFDGridManager
{
    // To reduce memory usage, no grid is stored for keyframes which are never used.
    // This dynamically creates a grid when requested.
    pub fn get_mut(&mut self, sample_time: SampleTime) -> Result<&mut FFDGrid, Error>
    {
        let last_sample_time = self.last_sample_time.ok_or_else(|| {
            anyhow!("Trying to grab FFD grid for a keyframe, but 0 keyframes exist.")
        })?;

        if sample_time > last_sample_time {
            Err(anyhow!(
                "Trying to grab FFD grid for a keyframe, but sample time {} exceeds max {}.",
                sample_time,
                last_sample_time
            ))
        }
        else {
            Ok(self
                .keyframes
                .entry(R32::from(sample_time))
                .or_insert(self.default.clone()))
        }
    }

    pub fn update_keyframe(&mut self, old_sample_time: Option<R32>, new_sample_time: Option<R32>)
    {
        let value = old_sample_time.and_then(|old_sample_time|
             self.keyframes.remove(&old_sample_time)
        );

        new_sample_time.and_then(|new_sample_time|{
            value.map(|value|(new_sample_time, value))
        }).and_then(|(k,v)|{
            self.keyframes.insert(k, v)
        });
    }

    pub fn load_keyframes(&mut self, save_data: FFDGridManagerSave) -> &mut Self
    {
        self.keyframes = save_data
            .keyframes
            .iter()
            .map(|(t, grid_save)| {
                (
                    R32::from(*t),
                    self.default.clone().load_and_apply_changes(grid_save)
                )
            })
            .collect();
        self.last_sample_time = save_data.last_possible_keyframe;

        self
    }

    pub fn apply(&self, physics_time: f32, vertices: Vec<vertex::Vertex>) -> Vec<vertex::Vertex>
    {
        let keyframe = self.keyframe_by_sample_time(physics_time);

        vertices
            .into_iter()
            .map(|vertex| {
                match keyframe {
                    Ok((_, grid)) => {
                        if grid.containing_cube_altered(&vertex) {
                            grid.apply(vertex)
                        }
                        else {
                            vertex
                        }
                    }
                    Err(((prev_t, prev_grid), (next_t, next_grid))) => {
                        if prev_grid.containing_cube_altered(&vertex)
                            || next_grid.containing_cube_altered(&vertex)
                        {
                            let current_grid = prev_grid.linear_interpolate_with(
                                next_grid,
                                (physics_time - prev_t) / (next_t - prev_t)
                            );
                            current_grid.apply(vertex)
                        }
                        else {
                            vertex
                        }
                    }
                }
            })
            .collect()
    }

    // 
    // - If the sample time matches a registered sample time exactly, the
    // corresponding keyframe is returned.
    // - If it does not match, but is valid within bounds, the 2 adjacent keyframes
    // are returned.
    // - If it's out of bounds (ex. past the last described keyframe), the last
    //   valid grid is returned.
    fn keyframe_by_sample_time<'a>(
        &'a self,
        t: SampleTime
    ) -> Result<FFDGridKeyFrame<'a>, (FFDGridKeyFrame<'a>, FFDGridKeyFrame<'a>)>
    {
        let mut prev_sample_time = 0.0;
        let mut prev_grid = &self.default;

        let mut keyframes = self.keyframes.iter().collect::<Vec<_>>();
        keyframes.sort_by_key(|(t, _)| *t);

        for (sample_time, grid) in keyframes.iter().map(|(k, v)| (*k, *v)) {
            if *sample_time == R32::from(t) {
                return Ok(((*sample_time).into(), grid))
            }
            else if t > prev_sample_time && t < (*sample_time).into() {
                return Err(((prev_sample_time, prev_grid), ((*sample_time).into(), grid)))
            }
            else {
                prev_sample_time = (*sample_time).into();
                prev_grid = grid;
            }
        }

        Ok((t, prev_grid))
    }
}

impl From<&Vec<Mesh>> for FFDGridManager
{
    fn from(meshes: &Vec<Mesh>) -> Self
    {
        Self {
            default:          FFDGrid::from(meshes),
            keyframes:        HashMap::new(),
            last_sample_time: Option::default()
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct FFDGridManagerSave
{
    keyframes:              Vec<(SampleTime, FFDGridSave)>,
    last_possible_keyframe: Option<SampleTime>
}

impl From<&FFDGridManager> for FFDGridManagerSave
{
    fn from(data: &FFDGridManager) -> Self
    {
        Self {
            keyframes:              data
                .keyframes
                .iter()
                .map(|(t, grid)| ((*t).into(), grid.into()))
                .collect::<Vec<_>>(),
            last_possible_keyframe: data.last_sample_time
        }
    }
}
