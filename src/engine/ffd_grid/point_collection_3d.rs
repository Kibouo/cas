use super::BOUNDING_SPHERE_RADIUS;
use crate::type_aliases::Vector3f;
use ncollide3d::bounding_volume::BoundingSphere;

#[derive(Clone)]
pub(super) struct PointCollection3D
{
    min_corner: (i32, i32, i32),
    data:       Vec<Vec<Vec<Option<BoundingSphere<f32>>>>>
}

impl PointCollection3D
{
    // Progress made by sample time is from self to other.
    pub fn linear_interpolate_with(&self, target: &PointCollection3D, factor: f32) -> Self
    {
        let self_dimensions = self.dimensions();
        let target_dimensions = target.dimensions();

        assert_eq!(
            self_dimensions, target_dimensions,
            "Per-point interpolation of clouds requires identically sized \
             dimensions.\nself.dimensions() = {:?}\ntarget.dimensions() = {:?}",
            self_dimensions, target_dimensions,
        );

        let mut data =
            vec![vec![vec![None; self_dimensions.2]; self_dimensions.1]; self_dimensions.0];
        self.iter().zip(target.iter()).for_each(
            |((start_idx, start_bound), (target_idx, target_bound))| {
                if start_bound.is_some() || target_bound.is_some() {
                    let start_coords = start_bound
                        .as_ref()
                        .map(|bound| Vector3f::from(*bound.center()))
                        .unwrap_or_else(|| {
                            Vector3f::new(
                                start_idx.0 as f32,
                                start_idx.1 as f32,
                                start_idx.2 as f32
                            )
                        });
                    let target_coords = target_bound
                        .as_ref()
                        .map(|bound| Vector3f::from(*bound.center()))
                        .unwrap_or_else(|| {
                            Vector3f::new(
                                target_idx.0 as f32,
                                target_idx.1 as f32,
                                target_idx.2 as f32
                            )
                        });

                    data[(start_idx.0 - self.min_corner.0) as usize]
                        [(start_idx.1 - self.min_corner.0) as usize]
                        [(start_idx.2 - self.min_corner.2) as usize] =
                        Some(BoundingSphere::<f32>::new(
                            (start_coords.clone() + (target_coords - start_coords) * factor).into(),
                            BOUNDING_SPHERE_RADIUS
                        ));
                }
            }
        );

        Self {
            min_corner: (
                (self.min_corner.0 as f32
                    + (target.min_corner.0 - self.min_corner.0) as f32 * factor)
                    .floor() as i32,
                (self.min_corner.1 as f32
                    + (target.min_corner.1 - self.min_corner.1) as f32 * factor)
                    .floor() as i32,
                (self.min_corner.2 as f32
                    + (target.min_corner.2 - self.min_corner.2) as f32 * factor)
                    .floor() as i32
            ),
            data
        }
    }

    pub fn len(&self) -> usize
    {
        let (x, y, z) = self.dimensions();
        x * y * z
    }

    pub fn dimensions(&self) -> (usize, usize, usize)
    {
        let x = self.data.len();
        let y = self
            .data
            .get(0)
            .expect("The grid is only 1D. Probably don't want that to happen.")
            .len();
        let z = self
            .data
            .get(0)
            .unwrap()
            .get(0)
            .expect("The grid is only 2D. Probably don't want that to happen.")
            .len();

        (x, y, z)
    }

    pub fn new(min_corner: (i32, i32, i32), max_corner: (i32, i32, i32)) -> Self
    {
        let data = vec![
            vec![
                vec![None; (max_corner.2 - min_corner.2 + 1) as usize];
                (max_corner.1 - min_corner.1 + 1) as usize
            ];
            (max_corner.0 - min_corner.0 + 1) as usize
        ];

        Self { min_corner, data }
    }

    pub fn get_mut(&mut self, idx: &(i32, i32, i32)) -> Option<&mut Option<BoundingSphere<f32>>>
    {
        let min_corner = &self.min_corner;
        self.data
            .get_mut((idx.0 - self.min_corner.0) as usize)
            .and_then(|x| x.get_mut((idx.1 - min_corner.1) as usize))
            .and_then(|y| y.get_mut((idx.2 - min_corner.2) as usize))
    }

    pub fn get(&self, idx: &(i32, i32, i32)) -> Option<&Option<BoundingSphere<f32>>>
    {
        self.data
            .get((idx.0 - self.min_corner.0) as usize)
            .and_then(|x| x.get((idx.1 - self.min_corner.1) as usize))
            .and_then(|y| y.get((idx.2 - self.min_corner.2) as usize))
    }

    pub fn iter(&self) -> impl Iterator<Item = ((i32, i32, i32), &Option<BoundingSphere<f32>>)>
    {
        self.data
            .iter()
            .enumerate()
            .map(move |(idx, col)| {
                col.iter()
                    .enumerate()
                    .map(move |(idy, dep)| {
                        dep.iter().enumerate().map(move |(idz, item)| {
                            (
                                (
                                    idx as i32 + self.min_corner.0,
                                    idy as i32 + self.min_corner.1,
                                    idz as i32 + self.min_corner.2
                                ),
                                item
                            )
                        })
                    })
                    .flatten()
            })
            .flatten()
    }
}
