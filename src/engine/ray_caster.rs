use super::projection_matrix;
use crate::type_aliases::{Isometry3f, Vector3f};
use ggez::Context;
use ncollide3d::query::{Ray, RayCast, RayIntersection};

#[derive(Default)]
pub struct RayCaster
{
    ray:      Option<Ray<f32>>,
    prev_ray: Option<Ray<f32>>
}

impl RayCaster
{
    // returns whether bounding volume was hit
    pub fn cast(
        &self,
        bounding_volume: Box<dyn RayCast<f32>>
    ) -> std::option::Option<RayIntersection<f32>>
    {
        bounding_volume.toi_and_normal_with_ray(
            &Isometry3f::identity(),
            self.ray.as_ref().expect("Failed to ray cast. No ray set."),
            f32::MAX,
            true
        )
    }

    pub fn set_ray(&mut self, ctx: &mut Context, camera_isometry: &Isometry3f) -> &mut Self
    {
        let (window_width_pixels, window_height_pixels) = ggez::graphics::drawable_size(ctx);
        let cursor_position = ggez::input::mouse::position(ctx);

        self.prev_ray = self.ray;
        self.ray = Some(Self::calc_pick_ray(
            window_width_pixels,
            window_height_pixels,
            cursor_position.x,
            cursor_position.y,
            camera_isometry
        ));

        self
    }

    fn calc_pick_ray(
        window_width_pixels: f32,
        window_height_pixels: f32,
        cursor_x: f32,
        cursor_y: f32,
        camera_isometry: &Isometry3f
    ) -> Ray<f32>
    {
        // viewport coords
        // watch out! Calculations assume origin left-bottom, while ggez returns
        // left-top
        let (cursor_x, cursor_y) = (cursor_x, window_height_pixels - cursor_y);

        // normalized device coords
        // each dimension is now in [-1:1]
        let ndc_x = ((2.0 * cursor_x) / window_width_pixels) - 1.0;
        let ndc_y = ((2.0 * cursor_y) / window_height_pixels) - 1.0;

        // homogeneous clip coords
        let clip = nalgebra::Vector4::<f32>::new(ndc_x, ndc_y, 1.0, 1.0);

        // eye coords
        let view_mat = camera_isometry.to_homogeneous();
        let proj_mat = projection_matrix().to_homogeneous();

        let mouse_ray = Vector3f::from((proj_mat * view_mat).try_inverse().unwrap() * clip);
        let camera_position = camera_isometry.translation();

        Ray::new(
            camera_position.clone().into(),
            *(mouse_ray - camera_position).inner()
        )
    }

    pub fn calc_point_translation(
        &mut self,
        grabbed_obj_intersection: RayIntersection<f32>
    ) -> Vector3f
    {
        Vector3f::from(
            self.prev_ray
                .expect("Trying to calculate drag with no previous ray set.")
                .point_at(grabbed_obj_intersection.toi)
        )
    }
}
