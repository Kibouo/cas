use ggez::event::{KeyCode, KeyMods, MouseButton};
use std::collections::{HashMap, HashSet};

#[derive(Default)]
pub struct MouseState
{
    pub position:        (f32, f32),
    pub pressed_buttons: HashSet<MouseButton>
}

#[derive(Default)]
pub struct KeyboardState
{
    pub pressed_keys: HashMap<KeyCode, bool>,
    pub pressed_mods: KeyMods
}
